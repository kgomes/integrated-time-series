// This is the definition for the data that is in the Midwater collection that is
// aggregated by year-month and depth

// Bring in the mongoose module to talk to MongoDB
var mongoose = require('mongoose');

// Create the api data schema
var midwaterAggByMonthDataSchema = new mongoose.Schema({
    "_id": {
        name: String,
        year: Number,
        month: Number,
        phylogeny: [
            {
                "level-one": String,
                "level-two": String,
                "level-three": String,
                superkingdom: String,
                kingdom: String,
                phylum: String,
                subphylum: String,
                superclass: String,
                class: String,
                subclass: String,
                order: String,
                suborder: String,
                family: String,
                subfamily: String,
                genus: String,
                species: String
            }
        ]
    },
    counts: Number
}, {
    collection: 'agg_mw_species_year_month'
});

// Create the MidwaterAggByMonthData model in mongoose
mongoose.model('MidwaterAggByMonthData', midwaterAggByMonthDataSchema)