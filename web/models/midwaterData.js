// This is the definition for the data that is in the Midwater collection

// Bring in the mongoose module to talk to MongoDB
var mongoose = require('mongoose');

// Create the api data schema
var midwaterDataSchema = new mongoose.Schema({
    "target-depth": Number,
    "date-resolution": String,
    "transect-start-date": Date,
    "transect-end-date": Date,
    year: Number,
    rov: String,
    ship: String,
    dive: String,
    "chief-scientist": String,
    "day-night": String,
    "transect-volume": Number,
    "recorded-date": Date,
    loc: {
        type: String,
        coordinates: [Number]
    },
    depth: Number,
    date: Date,
    "observation-date": Date,
    observer: String,
    name: String,
    phylogeny: [
        {
            "level-one": String,
            "level-two": String,
            "level-three": String,
            superkingdom: String,
            kingdom: String,
            phylum: String,
            superclass: String,
            subclass: String,
            order: String,
            family: String,
            genus: String,
            species: String
        }
    ],
    value: Number,
    "observation-id-fk": String,
    "link-name": String,
    "to-concept": String,
    "link-value": String,
    "associations": String,
    "video-archive-name": String
}, {
    collection: 'midwater_counts'
});

// Create the MidwaterData model in mongoose
mongoose.model('MidwaterData', midwaterDataSchema)