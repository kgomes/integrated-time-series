// This is the definition for the data that is in the Benthic collection

// Bring in the mongoose module to talk to MongoDB
var mongoose = require('mongoose');

// Create the benthic data schema
var benthicDataSchema = new mongoose.Schema({
    date: Date,
    "date-resolution": String,
    depth: Number,
    name: String,
    count: Number
}, {
    collection: 'benthic_counts'
});

// Create the BenthicData model in mongoose
mongoose.model('BenthicData', benthicDataSchema)