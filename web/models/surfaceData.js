// This is the definition for the data that is in the Surface collection

// Bring in the mongoose module to talk to MongoDB
var mongoose = require('mongoose');

// Create the surface data schema
var surfaceDataSchema = new mongoose.Schema({
    date: Date,
    "date-resolution": String,
    source: String,
    loc: {
        type: String,
        coordinates: [Number]
    },
    depth: Number,
    name: String,
    value: Number,
    units: String,
    "custom-properties": {
        cruise: String,
        expd: Number,
        seq: Number,
        bottle: Number,
        platform: String,
        ctrb_id: String,
        vf: Number,
        groups: Number,
        shape: Number,
        length: Number,
        width: Number,
        thick: Number,
        perliter: Number,
        volcell: Number,
        voliter: Number,
        carbcell: Number
    }
}, {
    collection: 'surface_data'
});

// Create the SurfaceData model in mongoose
mongoose.model('SurfaceData', surfaceDataSchema)