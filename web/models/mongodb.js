// This file configures the connection to MongoDB and it's behaviors

// Bring in the mongoose library for communication with MongoDB
var mongoose = require('mongoose');

// Bring in the winston logger
var winston = require('winston');

// Bring in the path module
var path = require('path');

// Export the function
module.exports = function (configuration, dataDirectory) {
    // Grab the logger level from the configuration or define a default
    var loggerLevel = configuration.loggerLevel || "info";

    // Create a new logger (I don't check for proper parent directories
    // because I am assuming the top level code has done that).
    var logger = new (winston.Logger)({
        transports: [
            new (winston.transports.File)({
                level: loggerLevel,
                filename: path.join(dataDirectory, 'logs', 'mongodb.log'),
                maxsize: configuration.maxLogFileSize
            })
        ]
    });
    logger.log('info',"MongoDB connection using URL " + configuration.mongodbURL);


    // Define a function to close mongodb gracefully when we get a shutdown
    var gracefulShutdown = function (msg, callback) {
        mongoose.connection.close(function () {
            logger.log('info',"Mongoose connection closed through " + msg);
            callback();
        });
    };

    // Set up some event handlers

    // When the connection to the MongoDB occurs
    mongoose.connection.on('connection', function () {
        logger.log('info',"MongoDB connected");
    });

    // When the connection is dropped to MongoDB
    mongoose.connection.on('disconnected', function () {
        logger.log('info',"MongoDB disconnected");
    });

    // When an error occurs with MongoDB
    mongoose.connection.on('error', function (err) {
        logger.log('error',"Something went wrong");
        logger.log('error', err);
    });

    // When a shutdown event occurs
    process.once('SIGUSR2', function () {
        gracefulShutdown('nodemon restart', function () {
            process.kill(process.pid, 'SIGUSR2');
        });
    });

    // When the application terminates
    process.on('SIGINT', function () {
        gracefulShutdown('app termination', function () {
            process.exit(0);
        });
    });

    // Connect to the DB
    mongoose.connect(configuration.mongodbURL);

    // Now require the data models
    require('./surfaceData');
    require('./surfaceAggByMonthData');
    require('./midwaterData');
    require('./midwaterAggByMonthData');
    require('./benthicData');
    require('./meiData');
    require('./tideData');
    require('./dayNightData');
    require('./ekmanData');

    // Now return the mongoose object
    return mongoose;
}
