// This is the definition for the data that is in the MEI collection

// Bring in the mongoose module to talk to MongoDB
var mongoose = require('mongoose');

// Create the mei data schema
var meiDataSchema = new mongoose.Schema({
    date: Date,
    "date-resolution": String,
    name: String,
    value: Number,
    units: String
}, {
    collection: 'mei_data'
});

// Create the MEIData model in mongoose
mongoose.model('MEIData', meiDataSchema)