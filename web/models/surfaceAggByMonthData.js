// This is the definition for the data that is in the Surface collection that is
// aggregated by year-month

// Bring in the mongoose module to talk to MongoDB
var mongoose = require('mongoose');

// Create the surface aggregation data schema
var surfaceAggByMonthDataSchema = new mongoose.Schema({
    "_id": {
        name: String,
        month: Number,
        year: Number
    },
    value: Number,
    date: Date
}, {
    collection: 'agg_surface_year_month'
});

// Create the SurfaceAggByMonthData model in mongoose
mongoose.model('SurfaceAggByMonthData', surfaceAggByMonthDataSchema)