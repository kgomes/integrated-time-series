// This is the definition for the data that is in the day night data collection

// Bring in the mongoose module to talk to MongoDB
var mongoose = require('mongoose');

// Create the day night data schema
var dayNightDataSchema = new mongoose.Schema({
    date: Date,
    "date-resolution": String,
    name: String,
    value: Number,
    units: String
}, {
    collection: 'daynight_data'
});

// Create the DayNightData Data model in mongoose
mongoose.model('DayNightData', dayNightDataSchema);