// This is the definition for the data that is in the tide data collection

// Bring in the mongoose module to talk to MongoDB
var mongoose = require('mongoose');

// Create the tide data schema
var tideDataSchema = new mongoose.Schema({
    date: Date,
    "date-resolution": String,
    name: String,
    value: Number,
    units: String
}, {
    collection: 'tide_data'
});

// Create the Tide Data model in mongoose
mongoose.model('TideData', tideDataSchema)