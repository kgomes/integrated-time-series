// This is the definition for the data that is in the Ekman data collection

// Bring in the mongoose module to talk to MongoDB
var mongoose = require('mongoose');

// Create the Ekman data schema
var ekmanDataSchema = new mongoose.Schema({
    date: Date,
    "date-resolution": String,
    name: String,
    value: Number,
    units: String
}, {
    collection: 'ekman_data'
});

// Create the Ekman Data model in mongoose
mongoose.model('EkmanData', ekmanDataSchema)