// Grab the ExpressJS router
var express = require('express');
var router = express.Router();

// Bring in the winston logger
var winston = require('winston');

// Bring in the path module
var path = require('path');

// This is the version of the API
var apiVersion = "";

// The directory where the log files will go
var dataDirectory;

// The level that the logger will record at
var loggerLevel;

// The maximum log file size
var maxLogFileSize;

// The logger
var logger;

// Surface data models
var SurfaceDataModel;
var SurfaceAggByMonthDataModel;

// Grab the Midwater model
var MidwaterDataModel;

// This is the api aggregated data model
var MidwaterAggByMonthDataModel;

// The benthic model
var BenthicDataModel;

// The MEI data model
var MEIDataModel;

// The Tide data model
var TideDataModel;

// The DayNight data model
var DayNightDataModel;

// The Ekman model
var EkmanDataModel;

module.exports = function (configuration) {
    // Grab the surface models
    SurfaceDataModel = configuration.surfaceDataModel;
    SurfaceAggByMonthDataModel = configuration.surfaceAggByMonthDataModel;

    // Grab the Midwater models
    MidwaterDataModel = configuration.midwaterDataModel;
    MidwaterAggByMonthDataModel = configuration.midwaterAggByMonthDataModel;

    // Grab the Benthic model
    BenthicDataModel = configuration.benthicDataModel;

    // Grab the MEI model
    MEIDataModel = configuration.meiDataModel;

    // Grab the Tide model
    TideDataModel = configuration.tideDataModel;

    // Grab the DayNight data model
    DayNightDataModel = configuration.dayNightDataModel;

    // The Ekman model
    EkmanDataModel = configuration.ekmanDataModel;

    // Grab the version number of the API
    apiVersion = configuration.apiVersion;

    // Now the data directory
    dataDirectory = configuration.dataDirectory;

    // Grab the logger level from the configuration or define a default
    loggerLevel = configuration.loggerLevel || "info";

    // Grab the maximum log file size or give it a default of 1 MB.
    maxLogFileSize = configuration.maxLogFileSize || 1000000;

    // Create a new logger (I don't check for proper parent directories
    // because I am assuming the top level code has done that).
    logger = new (winston.Logger)({
        transports: [
            new (winston.transports.File)({
                level: loggerLevel,
                filename: path.join(dataDirectory, 'logs', 'midwaterRouter.log'),
                maxsize: maxLogFileSize
            })
        ]
    });

    // This is the route that will respond to GET HTTP requests
    router.get('/:data', function (req, res, next) {
        logger.log('debug', 'A request for api data is being made');

        // First grab the data out of the URL that the client is looking for
        var dataType = req.params.data;

        // Let's first look for query parameters
        var species = req.query.species;
        var startDate = req.query.startDate;
        var localStartDate = Date.parse(startDate);
        var endDate = req.query.endDate;
        var localEndDate = Date.parse(endDate);
        var depth = req.query.depth;
        var localDepth = Number(depth);
        var startDepth = req.query.startDepth;
        var localStartDepth = Number(startDepth);
        var endDepth = req.query.endDepth;
        var localEndDepth = Number(endDepth);
        var binnedByMonth = req.query.binnedByMonth;
        var parameter = req.query.parameter;

        // Check to see if they want names only
        var namesOnly = req.query.namesOnly;

        // Check to see if the pylogeny level and query value were specified
        var phylogenyLevel = req.query.phylogenyLevel;
        var phylogenyParameter = req.query.phylogenyParameter;

        // Check to see if there are any other rare parameters
        var context = req.query.context;

        // The query that will be used
        var query = {};

        // Check for a date query
        if (localStartDate && !localEndDate) {
            query["date"] = {$ne: null, $gte: localStartDate}
        } else if (localEndDate && !localStartDate) {
            query["date"] = {$ne: null, $lte: localEndDate}
        } else if (localStartDate && localEndDate) {
            query["date"] = {$ne: null, $gte: localStartDate, $lte: localEndDate}
        } else {
            if (!binnedByMonth) {
                query["date"] = {$ne: null}
            }
        }

        // Check to see if the surface model was requested
        if (dataType == "surface") {
            // See if the request was for the names only
            if (namesOnly) {
                // Run the query for just the names
                SurfaceDataModel.find().distinct('name').exec(function (err, surfaceData) {
                    // Check for an error first
                    if (err) {
                        return res.json(err);
                    }

                    // The names come back in an array, so let's sort them
                    surfaceData.sort(function (a, b) {
                        return a.toLowerCase().localeCompare(b.toLowerCase());
                    });

                    // And now construct the response
                    var response = {
                        "apiVersion": "1.0.0"
                    };

                    // Check for context
                    if (context) {
                        response['context'] = context;
                    }

                    res.json(surfaceData);
                });
            } else {
                // Check to see if the user wants the data binned by month
                if (binnedByMonth) {
                    // Construct the query
                    if (parameter) {
                        query["_id.name"] = parameter
                    }
                    console.log("Query is ", query);
                    SurfaceAggByMonthDataModel.find(query).sort('_id.year _id.month').exec(function (err, surfaceAggByMonthData) {
                        // Check for the
                        if (err) return res.json(err);
                        res.json(surfaceAggByMonthData);
                    });
                } else {

                    // Check for each parameter
                    if (parameter) {
                        query["name"] = parameter;
                    }
                    // Run the query
                    console.log("Query is ", query);
                    SurfaceDataModel.find(query).sort('name date').exec(function (err, surfaceData) {
                        // Check for the
                        if (err) return res.json(err);
                        res.json(surfaceData);
                    });
                }
            }
        } else if (dataType == "midwater") {
            if (namesOnly) {
                // Check to see if they user is asking for phylogeny level names only
                if (phylogenyLevel) {
                    MidwaterDataModel.find().distinct('phylogeny.' + phylogenyLevel).exec(function (err, midwaterData) {
                        // What comes back is an array
                        midwaterData.sort();

                        // Check for the
                        if (err) return res.json(err);
                        res.json(midwaterData);
                    });
                } else {
                    MidwaterDataModel.find().distinct('name').exec(function (err, midwaterData) {
                        // What comes back is an array
                        midwaterData.sort();

                        // Check for the
                        if (err) return res.json(err);
                        res.json(midwaterData);
                    });
                }
            } else {
                // Check if the client wants it binned by month
                if (binnedByMonth) {
                    // Construct the query
                    if (species) {
                        query["_id.name"] = species;
                    } else if (phylogenyLevel && phylogenyParameter) {
                        query["_id.phylogeny." + phylogenyLevel] = phylogenyParameter;
                    }
                    console.log("Query is ", query);
                    MidwaterAggByMonthDataModel.find(query).sort({"_id.year":1, "_id.month":1}).exec(function (err, midwaterAggByMonthData) {
                        // Check for the
                        if (err) return res.json(err);
                        res.json(midwaterAggByMonthData);
                    });
                } else {

                    if (phylogenyLevel && phylogenyParameter) {
                        query["phylogeny." + phylogenyLevel] = phylogenyParameter;
                    } else if (species) {
                        query["name"] = species;
                    }

                    // Check to see if the depth was specified exactly
                    if (localDepth) {
                        query["depth"] = {$gte: localDepth - 10, $lte: localDepth + 10};
                    } else {
                        // Check to see if a depth range was specified
                        if (localStartDepth && !localEndDepth) {
                            query["depth"] = {$gte: localStartDepth}
                        } else if (localEndDepth && !localStartDepth) {
                            query["depth"] = {$lte: localEndDepth}
                        } else if (localStartDepth && localEndDepth) {
                            query["depth"] = {$gte: localStartDepth, $lte: localEndDepth}
                        }
                    }

                    // Run the query
                    console.log("Query is ", query);
                    MidwaterDataModel.find(query).select("name date value units").sort('date').exec(function (err, midwaterData) {
                        // Check for the
                        if (err) return res.json(err);
                        res.json(midwaterData);
                    });
                }
            }
        } else if (dataType == "benthic") {
            if (namesOnly) {
                BenthicDataModel.find().distinct('name').exec(function (err, benthicData) {
                    // What comes back is an array
                    benthicData.sort();

                    // Check for the
                    if (err) return res.json(err);
                    res.json(benthicData);
                });

            } else {
                if (species) {
                    query["name"] = species;
                }
                BenthicDataModel.find(query).sort({name: 1, date: 1}).exec(function (err, benthicData) {
                    // Check for the
                    if (err) return res.json(err);
                    res.json(benthicData);
                });
            }
        } else if (dataType == "mei") {
            MEIDataModel.find(query).sort({date: 1}).exec(function (err, meiData) {
                if (err) return res.json(err);
                res.json(meiData);
            });
        } else if (dataType == "tide") {
            TideDataModel.find(query).sort({date: 1}).exec(function (err, tideData) {
                if (err) return res.json(err);
                res.json(tideData);
            });
        } else if (dataType == "daynight") {
            DayNightDataModel.find(query).sort({date: 1}).exec(function (err, dayNightData) {
                if (err) return res.json(err);
                res.json(dayNightData);
            });
        } else if (dataType == "ekman") {
            EkmanDataModel.find(query).sort({date: 1}).exec(function (err, ekmanData) {
                if (err) return res.json(err);
                res.json(ekmanData);
            });
        } else {
            res.send("unknown data type");
        }
    }); // End router for HTTP GET at '/'


    return router;
};
