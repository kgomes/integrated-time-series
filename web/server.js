// This is the server entry point for the Integrated Time Series Project web application

// Read in the configuration file
var config = require('./config.js');

// Bring in the Winston logger for application logging
var winston = require('winston');

// Bring in the Morgan module to log access requests on the server
var morgan = require('morgan');

// Require the file-system module
var fs = require('fs');

// And the path module
var path = require('path');

// And a utility to make directories recursively
var mkdirp = require('mkdirp');

// Require the Express module for serving the application and APIs
var express = require('express');

// Bring in cookie parser module
var cookieParser = require('cookie-parser');

// Brin in body parser module
var bodyParser = require('body-parser');

// Bring in session support for the Express server
var session = require('express-session');

// Bring in the module that let's Express use MongoDB as it's persistent
// store for session information
var MongoStore = require('connect-mongo/es5')(session);

// Grab the data directory
var dataDirectory = config.dataDirectory;

// Die if the data directory not defined
if (!dataDirectory) {
    throw new Error("dataDirectory not set in config.js");
}
// Grab the default logger level
var loggerLevel = config.loggerLevel || "info";

// This is the winston logging directory
var winstonDirectory = path.join(dataDirectory, 'logs');

// The Winston logger
var logger;

// Create the MongoDB connection and load the appropriate models
var mongoose = require('./models/mongodb')(config.mongodb, dataDirectory);
mongoose.set('debug',true);

// Grab the surface models
var surfaceDataModel = mongoose.model('SurfaceData');
var surfaceAggByMonthDataModel = mongoose.model('SurfaceAggByMonthData');

// Grab the MidwaterData model
var midwaterDataModel = mongoose.model('MidwaterData');
var midwaterAggByMonthDataModel = mongoose.model('MidwaterAggByMonthData');

// Grab the Benthic model
var benthicDataModel = mongoose.model('BenthicData');

// Grab the MEI model
var meiDataModel = mongoose.model('MEIData');

// And the tide model
var tideDataModel = mongoose.model('TideData');

// And the daynight model
var dayNightDataModel = mongoose.model('DayNightData');

// The Ekman model
var ekmanDataModel = mongoose.model('EkmanData');

// Make sure the winston directory exists
if (fs.existsSync(winstonDirectory)) {
    logger = new (winston.Logger)({
        transports: [
            new (winston.transports.File)({
                level: loggerLevel,
                filename: path.join(winstonDirectory, 'web-server.log'),
                maxsize: config.maxLogFileSize
            })
        ]
    });
} else {
    // Create the directory, then the logger
    mkdirp(winstonDirectory, function (err) {
        // Check for errors
        if (err) {
            // Throw it
            throw err;
        } else {
            // Create the Winston logger
            logger = new (winston.Logger)({
                transports: [
                    new (winston.transports.File)({
                        level: loggerLevel,
                        filename: path.join(winstonDirectory, 'web-server.log'),
                        maxsize: config.maxLogFileSize
                    })
                ]
            });
        }
    });
}

// Create a log directory for the Morgan logger
var morganDirectory = path.join(dataDirectory, 'logs', 'web-server-access-logs');

// Grab a FileStreamRotator so the morgan logger can write access logs to a rotating file
var FileStreamRotator = require('file-stream-rotator');

// The Morgan log stream
var morganLogStream;

// Make sure the log directory for Morgan exists
if (fs.existsSync(morganDirectory)) {
    morganLogStream = FileStreamRotator.getStream({
        date_format: 'YYYYMMDD',
        filename: morganDirectory + '/access-%DATE%.log',
        frequency: 'daily',
        verbose: false
    });
} else {
    // It did not exist so create the new directory and then create the log stream
    mkdirp(morganDirectory, function (err) {
        if (err) {
            throw err;
        } else {
            morganLogStream = FileStreamRotator.getStream({
                date_format: 'YYYYMMDD',
                filename: morganDirectory + '/access-%DATE%.log',
                frequency: 'daily',
                verbose: false
            });
        }
    })
}

// Grab the API routes
var api = require('./routes/api')({
    surfaceDataModel: surfaceDataModel,
    surfaceAggByMonthDataModel: surfaceAggByMonthDataModel,
    midwaterDataModel: midwaterDataModel,
    midwaterAggByMonthDataModel: midwaterAggByMonthDataModel,
    benthicDataModel: benthicDataModel,
    meiDataModel: meiDataModel,
    tideDataModel: tideDataModel,
    dayNightDataModel: dayNightDataModel,
    ekmanDataModel: ekmanDataModel,
    apiVersion: config.apiVersion,
    dataDirectory: dataDirectory,
    loggerLevel: config.midwaterRouter.loggerLevel,
    maxLogFileSize: config.midwaterRouter.maxLogFileSize
});

// Create the new Express Application
var app = express();

// Add the morgan logger
app.use(morgan('combined', {stream: morganLogStream}));

// Add cookie parser
app.use(cookieParser());

// Add body parser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

// Define where the static content is served from
app.use(express.static(path.join(__dirname, 'public/dist')));

// Grab the session configuration
var sessionConfig = config.session;

// Add the MongoStore to store session information in MongoDB
sessionConfig["store"] = new MongoStore({
    mongooseConnection: mongoose.connection
});

// Add session support to Express and configure for security and
// to use the MongoDB for storage of session information
app.use(session(sessionConfig));

// Set the routes
app.use('/api/v1', api);

// Now listen on the specified port
var port = config.portNumber || 3000;
app.listen(port, function () {
    logger.log('info', 'Application started listening on port ' + port);
});
