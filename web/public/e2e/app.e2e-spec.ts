import { PublicNgCliPage } from './app.po';

describe('public-ng-cli App', () => {
  let page: PublicNgCliPage;

  beforeEach(() => {
    page = new PublicNgCliPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
