import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItsGraphComponentComponent } from './its-graph-component.component';

describe('ItsGraphComponentComponent', () => {
  let component: ItsGraphComponentComponent;
  let fixture: ComponentFixture<ItsGraphComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItsGraphComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItsGraphComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
