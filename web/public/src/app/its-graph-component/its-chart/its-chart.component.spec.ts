import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItsChartComponent } from './its-chart.component';

describe('ItsChartComponent', () => {
  let component: ItsChartComponent;
  let fixture: ComponentFixture<ItsChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItsChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItsChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
