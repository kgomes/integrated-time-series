import {Component, OnInit} from '@angular/core';
import * as Highcharts from 'highcharts';
require('highcharts/highcharts-more')(Highcharts);
require('highcharts/modules/exporting')(Highcharts);

@Component({
  selector: 'app-its-chart',
  templateUrl: './its-chart.component.html',
  styleUrls: ['./its-chart.component.css']
})
export class ItsChartComponent implements OnInit {

  chart: any;

  constructor() {
  }

  ngOnInit() {
    this.chart = new Highcharts.Chart(
      {
        chart: {
          renderTo: 'graph-container',
          zoomType: 'x'
        },
        title: {
          text: 'Integrated Time Series'
        },
        subtitle: {
          text: 'Surface, Midwater and Benthic Data'
        },
        xAxis: {
          type: 'datetime',
          title: 'Date',
        }
      }
    );
  }

  addYAxis(newYAxis: any) {
    this.chart.addAxis(newYAxis);
  }

  addDataSet(newSeries: any) {
    this.chart.addSeries(newSeries);
  }

  removeDataSet(seriesID: string, axisID: string) {
    this.chart.get(seriesID).remove();
    this.chart.get(axisID).remove();
  }

  addPlotBand(bandID: string, startDate: any, endDate: any, color: string) {
    this.chart.xAxis[0].addPlotBand({
      id: bandID,
      color: color,
      from: startDate,
      to: endDate
    });
  }

  removePlotBand(bandID: string) {
    this.chart.xAxis[0].removePlotBand(bandID);
  }

  changeSeriesType(seriesID: string, chartType: string) {
    this.chart.get(seriesID).update({
      type: chartType
    });
  }

  changeYAxis(seriesID: string, property: string, value: string) {
    if (property === 'min') {
      this.chart.get(seriesID + '-axis').setExtremes(parseFloat(value), null);
    } else if (property === 'max') {
      this.chart.get(seriesID + '-axis').setExtremes(null, parseFloat(value));
    } else if (property === 'log') {
      // Check to see if log was chosen
      if (value) {
        this.chart.get(seriesID + '-axis').update(
          {
            type: 'logarithmic',
            minorTickInterval: 'auto'
          }
        );
      } else {
        this.chart.get(seriesID + '-axis').update(
          {type: 'linear'}
        );
      }
    } else if (property === 'color-change') {
      this.chart.get(seriesID).update(
        {
          color: value
        }
      );
      this.chart.get(seriesID + '-axis').update({
        lineColor: value
      });
    }
  }

  showLoading() {
    this.chart.showLoading();
  }

  hideLoading() {
    this.chart.hideLoading();
  }
}
