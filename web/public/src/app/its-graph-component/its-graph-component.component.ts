import {Component, OnInit, ViewChild} from '@angular/core';
import {DataServiceService} from '../data-service.service';
import {ItsChartComponent} from './its-chart/its-chart.component';
import {DataSetTableComponent} from './data-set-table/data-set-table.component';
import * as Math from 'mathjs';

@Component({
  selector: 'app-its-graph-component',
  templateUrl: './its-graph-component.component.html',
  styleUrls: ['./its-graph-component.component.css']
})
export class ItsGraphComponentComponent implements OnInit {

  // This is the graph component handle needed for event communication
  @ViewChild(ItsChartComponent)
  private itsChartComponent: ItsChartComponent;

  // This is the data series table
  @ViewChild(DataSetTableComponent)
  private itsDataSetTable: DataSetTableComponent;

  // This is the collection of data sets that the user has selected
  dataSets = {};

  // This is the collection of y axes
  yAxes = {};

  constructor(private dataService: DataServiceService) {
  }

  ngOnInit() {
    this.itsDataSetTable.setDataSeriesCollection(this.dataSets);
  }

  // A method to generate random colors for the chart
  generateRandomColors() {
    // Some hex letters to use as the seed for the random color
    const letters = '0123456789ABCDEF'.split('');

    // Define the color and create the hex code
    let color = '#';
    for (let i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }

    // And return it
    return color;
  }

  // The method that handles when a user clicks a check box
  handleDataSelection(type: string, phylogenyLevel: string, name: string, checked: boolean) {
    console.log('User selected ' + name + ' from ' + type + ' and selection is now ' + checked);

    this.itsChartComponent.showLoading();

    // Create the series ID and the yAxis
    let seriesID = type;
    if (name) {
      seriesID = seriesID + '-' + name;
    }
    let seriesAxisID = type;
    if (name) {
      seriesAxisID = seriesAxisID + '-' + name;
    }
    seriesAxisID = seriesAxisID + '-axis';

    // Check to see if the user selected or de-selected the data
    if (checked) {
      // Create the data series
      const newSeries = {};

      // Create the color for the new Axis and Series
      const seriesColor = this.generateRandomColors();

      // Create a new y Axis
      const newYAxis = {
        id: seriesAxisID,
        title: {
          text: name
        },
        minorTickInterval: 'auto',
        lineWidth: 2,
        lineColor: seriesColor
      };

      // Add it to the local collection of Axes
      this.yAxes[seriesAxisID] = newYAxis;

      // Add it to the chart
      if (type !== 'daynight') {
        this.itsChartComponent.addYAxis(newYAxis);
      }

      // Create the ID
      newSeries['id'] = seriesID;

      // Give it a name
      newSeries['name'] = name;

      // And the type
      newSeries['its-data-type'] = type;

      // Make sure it's visible
      newSeries['visible'] = true;

      // Add the color
      newSeries['color'] = seriesColor;

      // Add it to the series
      newSeries['yAxis'] = seriesAxisID;

      // Check to see what kind of data this is
      if (type === 'surface') {

        // Grab the name of the surface data
        this.dataService.getSurfaceData(name).subscribe(
          data => {
            console.log('Got data for ' + name);

            // An array to hold the data
            const seriesData = [];

            // Add the data to the seriesData Array
            data.forEach(function (dataPoint) {
              // Grab the date
              const dateForPoint = new Date(dataPoint.date);
              seriesData.push([dateForPoint.getTime(), dataPoint.value]);
            });

            // Set the data on the series
            newSeries['data'] = seriesData;

            // Now add the series and axis to the local objects for working with in the future
            this.dataSets[seriesID] = newSeries;

            // Now add it to the chart
            this.itsChartComponent.addDataSet(newSeries);
            this.itsChartComponent.hideLoading();
          },
          error => console.log(error)
        );
      } else if (type === 'midwater') {
        this.dataService.getMidwaterData(phylogenyLevel, name).subscribe(
          data => {
            console.log('Got data for ' + name + ' and there are ' + data.length + ' points');

            // An array to hold the data
            const seriesData = [];

            // Add the data to the seriesData Array
            data.forEach(function (dataPoint) {
              // Create a new date
              const dateForPoint = new Date();
              dateForPoint.setFullYear(dataPoint['_id'].year, dataPoint['_id'].month, 1);
              seriesData.push([dateForPoint.getTime(), dataPoint.counts]);
            });

            // Set the data on the series
            newSeries['data'] = seriesData;

            // Make it column by default
            newSeries['type'] = 'column';

            // Now add the series and axis to the local objects for working with in the future
            this.dataSets[seriesID] = newSeries;

            // Now add it to the chart
            this.itsChartComponent.addDataSet(newSeries);
            this.itsChartComponent.hideLoading();
          },
          error => console.log(error)
        );
      } else if (type === 'benthic') {

        // Grab the name of the surface data
        this.dataService.getBenthicData(name).subscribe(
          data => {
            console.log('Got data for ' + name);

            // An array to hold the data
            const seriesData = [];

            // Add the data to the seriesData Array
            data.forEach(function (dataPoint) {
              // Grab the date
              const dateForPoint = new Date(dataPoint.date);
              seriesData.push([dateForPoint.getTime(), dataPoint.value]);
            });

            // Set the data on the series
            newSeries['data'] = seriesData;

            // Set to column to start
            newSeries['type'] = 'column';

            // Now add the series and axis to the local objects for working with in the future
            this.dataSets[seriesID] = newSeries;

            // Now add it to the chart
            this.itsChartComponent.addDataSet(newSeries);
            this.itsChartComponent.hideLoading();
          },
          error => console.log(error)
        );
      } else if (type === 'tide') {

        // Grab the name of the surface data
        this.dataService.getTideData().subscribe(
          data => {
            console.log('Got data for ' + name);

            // An array to hold the data
            const seriesData = [];

            // Add the data to the seriesData Array
            data.forEach(function (dataPoint) {
              // Grab the date
              const dateForPoint = new Date(dataPoint.date);
              seriesData.push([dateForPoint.getTime(), dataPoint.value]);
            });

            // Set the data on the series
            newSeries['data'] = seriesData;

            // Graph type should be spline
            newSeries['type'] = 'spline';

            // Now add the series and axis to the local objects for working with in the future
            this.dataSets[seriesID] = newSeries;

            // Now add it to the chart
            this.itsChartComponent.addDataSet(newSeries);
            this.itsChartComponent.hideLoading();
          },
          error => console.log(error)
        );
      } else if (type === 'daynight') {

        // Grab the name of the daynight data
        this.dataService.getDayNightData().subscribe(
          data => {
            console.log('Got data for ' + name);

            // Some variables to keep track of dates
            let sunriseDate;
            let sunsetDate;

            // A band counter
            let bandCounter = 1;

            // Grab a handle to the execution context
            const me = this;

            // So for day night data, it's a bit different, we basically just add plot bands for daylight hours
            data.forEach(function (dataPoint) {
              // Check to see if it's sunrise or sunset
              if (dataPoint.value === 1) {
                sunriseDate = new Date(dataPoint.date);
              } else {
                sunsetDate = new Date(dataPoint.date);
                // We should now have a band, let's add one
                if (sunriseDate) {
                  me.itsChartComponent.addPlotBand('daynight', sunriseDate, sunsetDate, '#FCFFC5');
                  bandCounter = bandCounter + 1;
                }
              }
            });

            this.itsChartComponent.hideLoading();
          },
          error => console.log(error)
        );
      } else if (type === 'mei') {

        // Grab the name of the surface data
        this.dataService.getMEIData().subscribe(
          data => {
            console.log('Got data for ' + name);

            // An array to hold the data
            const seriesData = [];

            // Add the data to the seriesData Array
            data.forEach(function (dataPoint) {
              // Grab the date
              const dateForPoint = new Date(dataPoint.date);
              seriesData.push([dateForPoint.getTime(), dataPoint.value]);
            });

            // Set the data on the series
            newSeries['data'] = seriesData;

            newSeries['type'] = 'area';
            newSeries['color'] = 'rgba(255,0,0,0.5)';
            newSeries['negativeColor'] = 'rgba(0,0,255,0.5)';

            // Now add the series and axis to the local objects for working with in the future
            this.dataSets[seriesID] = newSeries;

            // Now add it to the chart
            this.itsChartComponent.addDataSet(newSeries);
            this.itsChartComponent.hideLoading();

          },
          error => console.log(error)
        );
      } else if (type === 'ekman') {

        // Grab the name of the surface data
        this.dataService.getEkmanData().subscribe(
          data => {
            console.log('Got data for ' + name);

            // An array to hold the data
            const seriesData = [];

            // Add the data to the seriesData Array
            data.forEach(function (dataPoint) {
              // Grab the date
              const dateForPoint = new Date(dataPoint.date);
              seriesData.push([dateForPoint.getTime(), dataPoint.value]);
            });

            // Set the data on the series
            newSeries['data'] = seriesData;

            // Graph type should be spline
            newSeries['type'] = 'line';

            // Now add the series and axis to the local objects for working with in the future
            this.dataSets[seriesID] = newSeries;

            // Now add it to the chart
            this.itsChartComponent.addDataSet(newSeries);
            this.itsChartComponent.hideLoading();
          },
          error => console.log(error)
        );
      }

    } else {
      if (type === 'daynight') {
        this.itsChartComponent.removePlotBand('daynight');
      } else {
        this.itsChartComponent.removeDataSet(seriesID, seriesAxisID);
        delete this.dataSets[seriesID];
        delete this.yAxes[seriesAxisID];
      }
      this.itsChartComponent.hideLoading();
    }

  }

  // The method to handle the user changing chart types
  handleChartTypeChange(event: any) {
    // Find the correct series and update the chart type
    this.itsChartComponent.changeSeriesType(event.seriesID, event.chartType);
  }

  // The method to handle changes to Y axis
  handleYAxisChange(event: any) {
    // The value to pass to the y-axis change
    let valueToPass = event.value;
    if (event.property === 'color-change') {
      valueToPass = this.generateRandomColors();
    }
    this.itsChartComponent.changeYAxis(event.seriesID, event.property, valueToPass);
  }

  // This function creates a new data series by integrating an existing series over some interval
  handleIntegrationRequest(event: any) {
    this.itsChartComponent.showLoading();

    // Grab the series ID
    const integrationSeriesID = event.seriesID;
    const checked = event.checked;
    const value = event.value;

    const intSeriesID = integrationSeriesID + '-integration';
    const intSeriesAxisID = intSeriesID + '-axis';

    // Check to see if it's checked or unchecked
    console.log('checked = ' + checked);

    // See if the check is on or off
    if (checked) {

      // Check to make sure there is some integral value
      if (value) {
        // Convert to number
        const valueNumber = parseFloat(value);
        console.log('value string is ' + value + ' and converted to number is ' + valueNumber);
        if (valueNumber && (valueNumber > 0)) {
          console.log('integral value is good to go');
          // Grab the existing data series
          const existingSeries = this.dataSets[integrationSeriesID];

          if (existingSeries) {
            // Create a new series
            const newIntSeries = {};

            // Create the color for the new Axis and Series
            const intSeriesColor = this.generateRandomColors();

            // Create a new y Axis
            const newIntYAxis = {
              id: intSeriesAxisID,
              title: {
                text: 'Integration'
              },
              minorTickInterval: 'auto',
              lineWidth: 2,
              lineColor: intSeriesColor
            };

            // Add it to the local collection of Axes
            this.yAxes[intSeriesAxisID] = newIntYAxis;

            // Add it to the chart
            this.itsChartComponent.addYAxis(newIntYAxis);

            // Create the ID
            newIntSeries['id'] = intSeriesID;

            // Give it a name
            newIntSeries['name'] = 'Integration';

            // And the type
            newIntSeries['its-data-type'] = 'Integration';

            // Make sure it's visible
            newIntSeries['visible'] = true;

            // Add the color
            newIntSeries['color'] = intSeriesColor;

            // Add it to the series
            newIntSeries['yAxis'] = intSeriesAxisID;

            // Now loop over the existing series and calculate the integral values
            const intData = [];
            const existingValues = [];
            const integralValues = [];

            // Make a pass over the existing and calculate integral
            for (let i = 0; i < existingSeries['data'].length; i++) {
              const existingDate = existingSeries['data'][i][0];
              const existingValue = existingSeries['data'][i][1];
              // Add number to arrays for integral calculations
              existingValues.push(existingValue);
              if (i === 0) {
                integralValues.push(existingValue);
              } else {
                integralValues.push(integralValues[i - 1] * (1 - (1 / valueNumber)) + existingValue);
              }
            }

            // Calculate standard deviation for existing values
            const stdExisting = Math.std(existingValues);
            const stdIntegral = Math.std(integralValues);
            const avgIntegral = Math.mean(integralValues);
            console.log('Existing std: ' + stdExisting + ', integral std: ' + stdIntegral + ', avg integral: ' + avgIntegral);

            // Now we need to loop over existing data and create new series with normalized integral data
            for (let j = 0; j < existingSeries['data'].length; j++) {
              intData.push([existingSeries['data'][j][0], (integralValues[j] - avgIntegral) / (stdExisting * stdIntegral)]);
            }

            newIntSeries['data'] = intData;

            this.itsChartComponent.addDataSet(newIntSeries);
          }
        }
      }
    } else {
      this.itsChartComponent.removeDataSet(intSeriesID, intSeriesAxisID);
      delete this.dataSets[intSeriesID];
      delete this.yAxes[intSeriesAxisID];
    }

    this.itsChartComponent.hideLoading();
  }
}
