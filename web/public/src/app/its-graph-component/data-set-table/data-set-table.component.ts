import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {min} from 'rxjs/operator/min';

@Component({
  selector: 'app-data-set-table',
  templateUrl: './data-set-table.component.html',
  styleUrls: ['./data-set-table.component.css']
})
export class DataSetTableComponent implements OnInit {

  // This is the reference to the collection of data series that will be represented in the table
  dataSeriesCollection = {};

  // This is an array of the values in the integration inputs
  integrationInputValues = [];

  // This is the event emitter to send selection events to the parent container
  @Output() notifyChartTypeChange: EventEmitter<any> = new EventEmitter<any>();

  // This is to send events about y axis changes
  @Output() notifyYAxisChange: EventEmitter<any> = new EventEmitter<any>();

  @Output() notifyIntegrationRequest: EventEmitter<any> = new EventEmitter<any>();

  constructor() {
  }

  ngOnInit() {
  }

  getDataSeriesIDs() {
    return Object.keys(this.dataSeriesCollection);
  }

  // The method to set the data series
  setDataSeriesCollection(dataSeriesCollection) {
    this.dataSeriesCollection = dataSeriesCollection;
  }

  handleChartTypeSelection(seriesID: string, chartType: any) {
    this.notifyChartTypeChange.emit({
      seriesID: seriesID,
      chartType: chartType
    });
  }

  handleYAxisChange(seriesID: string, property: string, value: string) {
    if (value !== '') {
      console.log('data table detected event for ' + property + ' for series ' + seriesID + ' and value is ' + value);
      this.notifyYAxisChange.emit({
        seriesID: seriesID,
        property: property,
        value: value
      });
    }
  }

  handleColorChange(seriesID: string) {
    this.notifyYAxisChange.emit({
      seriesID: seriesID,
      property: 'color-change',
      value: ''
    });
  }

  handleIntegrationRequest(seriesID: string, checked: string, intervalIndex: number) {
    console.log('Integration request for ' + seriesID + ' checked? ' + checked + ' with value of ' +
      this.integrationInputValues[intervalIndex]);
    this.notifyIntegrationRequest.emit({
      seriesID: seriesID,
      checked: checked,
      value: this.integrationInputValues[intervalIndex]
    });
  }
}
