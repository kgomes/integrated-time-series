import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class DataServiceService {

  constructor(private httpService: Http) {
  }

  getSurfaceNames() {
    // Now set up the HTTP call
    return this.httpService.get('api/v1/surface?namesOnly=true').map(response => response.json());
  }

  getSurfaceData(name: string) {
    return this.httpService.get('api/v1/surface?parameter=' + name).map(response => response.json());
  }

  getMidwaterNamesAtLevel(levelName: string) {
    return this.httpService.get('api/v1/midwater?namesOnly=true&phylogenyLevel=' + levelName).map(response => response.json());
  }

  getMidwaterData(levelName: string, phylogenyName: string) {
    return this.httpService.get('api/v1/midwater?phylogenyLevel=' + levelName +
      '&phylogenyParameter=' + phylogenyName + '&binnedByMonth=true').map(response => response.json());
  }

  getBenthicNames() {
    // Now set up the HTTP call
    return this.httpService.get('api/v1/benthic?namesOnly=true').map(response => response.json());
  }

  getBenthicData(name: string) {
    return this.httpService.get('api/v1/benthic?species=' + name).map(response => response.json());
  }

  getMEIData() {
    return this.httpService.get('api/v1/mei').map(response => response.json());
  }

  getTideData() {
    return this.httpService.get('api/v1/tide').map(response => response.json());
  }

  getDayNightData() {
    return this.httpService.get('api/v1/daynight').map(response => response.json());
  }

  getEkmanData() {
    return this.httpService.get('api/v1/ekman').map(response => response.json());
  }

}
