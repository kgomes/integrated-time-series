import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { ItsSelectorComponent } from './its-selector/its-selector.component';
import { ItsGraphComponentComponent } from './its-graph-component/its-graph-component.component';
import { DataSetTableComponent } from './its-graph-component/data-set-table/data-set-table.component';
import { ItsChartComponent } from './its-graph-component/its-chart/its-chart.component';
import { ChartStatsComponent } from './its-graph-component/chart-stats/chart-stats.component';

@NgModule({
  declarations: [
    AppComponent,
    ItsSelectorComponent,
    ItsGraphComponentComponent,
    DataSetTableComponent,
    ItsChartComponent,
    ChartStatsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
