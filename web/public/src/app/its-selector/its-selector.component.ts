import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {DataServiceService} from '../data-service.service';

@Component({
  selector: 'app-its-selector',
  templateUrl: './its-selector.component.html',
  styleUrls: ['./its-selector.component.css']
})
export class ItsSelectorComponent implements OnInit {

  // The height of the selection list
  listMaxHeight: string;

  // These are the arrays of strings for the selection lists
  surfaceList: string[];
  midwaterList: string[];
  phylogenyLevel: string;
  benthicList: string[];

  // This is the event emitter to send selection events to the parent container
  @Output() notifySelection: EventEmitter<any> = new EventEmitter<any>();

  constructor(private dataService: DataServiceService) {

  }

  ngOnInit() {
    this.setListHeight();
    this.phylogenyLevel = 'species';
    // Grab the name of the surface data
    this.dataService.getSurfaceNames().subscribe(
      data => {
        this.surfaceList = data;
      },
      error => console.log(error)
    );
    // Grab an initial midwater list
    this.dataService.getMidwaterNamesAtLevel('species').subscribe(
      data => {
        this.midwaterList = data;
      },
      error => console.log(error)
    );
    // Grab an benthic list
    this.dataService.getBenthicNames().subscribe(
      data => {
        this.benthicList = data;
      },
      error => console.log(error)
    );
  }

  setListHeight() {
    this.listMaxHeight = (window.innerHeight - 290) + 'px';
  }

  handleSelectionClick(sourceType: string, name: string, checked: boolean) {
    console.log('sourceType=' + sourceType + ',name=' + name + ',phylogenyLevel=' + this.phylogenyLevel + ',checked=' + checked);
    this.notifySelection.emit({
      sourceType: sourceType,
      phylogenyLevel: this.phylogenyLevel,
      name: name,
      checked: checked
    });
  }

  handlePhylogenyLevelSelect(phylogenyLevel: string) {
    // Set the current phylogeny level
    this.phylogenyLevel = phylogenyLevel;
    this.dataService.getMidwaterNamesAtLevel(phylogenyLevel).subscribe(
      data => {
        this.midwaterList = data;
      },
      error => console.log(error)
    );
  }

}
