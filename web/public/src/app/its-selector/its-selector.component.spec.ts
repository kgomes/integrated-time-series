import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItsSelectorComponent } from './its-selector.component';

describe('ItsSelectorComponent', () => {
  let component: ItsSelectorComponent;
  let fixture: ComponentFixture<ItsSelectorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItsSelectorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItsSelectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
