import {Component, ViewChild} from '@angular/core';
import {DataServiceService} from './data-service.service';
import {ItsGraphComponentComponent} from './its-graph-component/its-graph-component.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [
    DataServiceService
  ]
})
export class AppComponent {

  // This is the title that goes in the navbar
  title = 'Integrated Time Series Visualization';

  // This is the class that is used to toggle the side menu
  menuToggled = false;

  // This is the graph component handle needed for event communication
  @ViewChild(ItsGraphComponentComponent)
  private itsGraphComponent: ItsGraphComponentComponent;

  // This is the function that toggles the sidemenu
  toggleMenu(): void {
    this.menuToggled = !this.menuToggled;
  }

  handleDataSelection(event): void {
    this.itsGraphComponent.handleDataSelection(event.sourceType, event.phylogenyLevel, event.name, event.checked);
  }
}
