package org.mbari.its;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.*;
import org.bson.Document;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by kgomes on 10/19/15.
 */
public class BenthicExtractor {

    /**
     * The logger for this class
     */
    private static final Logger logger = LogManager.getLogger(BenthicExtractor.class);

    /**
     * This is the path to the spreadsheet with the benthic data
     */
    private String benthicSpreadsheetPath = null;

    /**
     * The MongoDB collection that will be written to
     */
    private MongoCollection mongoCollection = null;

    /**
     * The constructor
     */
    public BenthicExtractor(String pathToSpreadsheet, MongoCollection mongoCollection) {
        logger.debug("BenthicExtractor constructed using path " + pathToSpreadsheet);
        this.benthicSpreadsheetPath = pathToSpreadsheet;
        this.mongoCollection = mongoCollection;
    }

    /**
     * Method to read in the data from the spreadsheet into JSON objects
     */
    public void extractData() {
        logger.debug("Starting benthic data extraction");
        try {
            // For now, drop the collection
            mongoCollection.drop();

            /**
             * List of Documents to insert
             */
            List<Document> dataPoints = new ArrayList<>();

            // Create the file input stream for the Excel spreadsheet
            InputStream inp = new FileInputStream(benthicSpreadsheetPath);

            // Create the workbook from the file input stream
            Workbook wb = WorkbookFactory.create(inp);

            // Grab the second sheet which is where the data is located
            Sheet sheet = wb.getSheetAt(1);

            // Set the looping flag
            boolean rowLoop = true;

            // Start on the second row (skip the header)
            int rowCounter = 1;

            // Now loop over each row (depth)
            while (rowLoop) {
                // Grab the row
                Row row = sheet.getRow(rowCounter);

                // Grab the transect length cell
                Cell cell = row.getCell(2);

                // Make sure it's still OK to use
                if (cell != null && cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {

                    // Grab the date
                    Cell dateCell = row.getCell(1);

                    // Cell flag and counter
                    boolean cellLoop = true;
                    int cellCounter = 3;

                    // Now loop over all the rest of the columns
                    while (cellLoop) {

                        // Grab the cell
                        Cell countCell = row.getCell(cellCounter);

                        // Make sure it's a number and it exists
                        if (countCell == null || countCell.getCellType() != Cell.CELL_TYPE_NUMERIC) {
                            cellLoop = false;
                        } else {
                            // If the count is greater than zero
                            if (countCell.getNumericCellValue() > 0) {
                                // Create the BSON Document
                                Document document = new Document("date", dateCell.getDateCellValue())
                                        .append("date-resolution", "month")
                                        .append("depth", 4000)
                                        .append("name", sheet.getRow(0).getCell(cellCounter).getStringCellValue())
                                        .append("value", countCell.getNumericCellValue())
                                        .append("units", "counts");
                                dataPoints.add(document);
                            }
                        }
                        // Bump the counter
                        cellCounter++;
                    }

                    // Bump the row counter
                    rowCounter++;
                } else {
                    rowLoop = false;
                }
            }

            // Now insert all the documents
            mongoCollection.insertMany(dataPoints);
        } catch (Exception ioe) {
            ioe.printStackTrace();
        }
        logger.debug("Done with benthic extraction");
    }
}
