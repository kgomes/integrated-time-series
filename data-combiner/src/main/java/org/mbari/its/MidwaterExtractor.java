package org.mbari.its;

import com.mongodb.client.MongoCollection;
import com.mongodb.util.JSON;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bson.Document;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static java.util.Arrays.asList;

/**
 * This class serves as the interface to the VARS database that we want to extract information from
 */
public class MidwaterExtractor {

    /**
     * The logger for this class
     */
    private static final Logger logger = LogManager.getLogger(MidwaterExtractor.class);

    /**
     * This is the connection URL to the Midwater database
     */
    private String midwaterConnectionUrl = null;
    /**
     * This is the connection URL to the VARS database
     */
    private String varsConnectionUrl = null;

    /**
     * The connection URL to the VARS Knowledgebase (for phylogeny queries)
     */
    private String varsKBConnectionUrl = null;

    /**
     * The base URL for the phylogeny search
     */
    private String phylogenyUrlBase = null;

    /**
     * The list of concepts that will be queried for in the midwater
     */
    private String[] conceptList = null;

    /**
     * This is where this extractor will writes its result files to
     */
    private File dataDirectory = null;

    /**
     * This is the file where the ROV start and end date are pulled from the Midwater database
     */
    private File midwaterExtractionFile = null;

    /**
     * This is the file where VARS annotations for the ROV dives will be written
     */
    private File varsAnnotationsFile = null;

    /**
     * This is the file where the possible duplicate annotations will be written
     */
    private File possibleDuplicateAnnotationsFile = null;

    /**
     * This is the file which has the VARS annotations, but with any duplicates removed
     */
    private File varsAnnotationsFileDuplicatesRemoved = null;

    /**
     * This is the MongoDB collection that the extractor will write to
     */
    private MongoCollection mongoCollection = null;

    /**
     * A date formatter for parsing
     */
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");

    /**
     * The date format for the date stamps from midwater queries
     */
    private SimpleDateFormat midwaterDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");

    /**
     * The constructor that takes in the varsConnectionUrl
     *
     * @param varsConnectionUrl
     */
    public MidwaterExtractor(String midwaterConnectionUrl, String varsConnectionUrl, String varsKBConnectionUrl,
                             String phylogenyUrlBase, String[] conceptList, File dataDirectory,
                             MongoCollection mongoCollection) {

        // Set the connection Urls
        this.midwaterConnectionUrl = midwaterConnectionUrl;
        this.varsConnectionUrl = varsConnectionUrl;
        this.varsKBConnectionUrl = varsKBConnectionUrl;

        // They phylogeny base url
        this.phylogenyUrlBase = phylogenyUrlBase;
        logger.debug("Phylogeny URL Base = " + this.phylogenyUrlBase);

        // Set the concept list if there was one
        if (conceptList != null) {
            this.conceptList = conceptList;
        }

        // Set the data directory
        this.dataDirectory = dataDirectory;

        // And the MongoDB collection
        this.mongoCollection = mongoCollection;

        // Create a FileWriter for the ROV start and end dates
        this.midwaterExtractionFile = new File(this.dataDirectory + File.separator + "midwaterExtraction.csv");

        // And for the VARS annotations
        this.varsAnnotationsFile = new File(this.dataDirectory + File.separator + "varsAnnotationsExtraction.csv");

        // And the possible duplicates file
        this.possibleDuplicateAnnotationsFile = new File(this.dataDirectory + File.separator +
                "varsPossibleDuplicateAnnotations.csv");

        // And the VARS annotations with duplicates removed
        this.varsAnnotationsFileDuplicatesRemoved = new File(this.dataDirectory + File.separator +
                "varsAnnotationsExtractionDuplicatesRemoves.csv");

    }

    /**
     * This method attempt to establish a connection to the VARS database and reports back if it was
     * successful (true) or not (false)
     *
     * @return
     */
    public boolean testConnection() {
        // Create a SQL connection
        Connection connection = null;

        try {
            connection = DriverManager.getConnection(this.midwaterConnectionUrl);
        } catch (SQLException e) {
            logger.error("SQLException caught on connection: " + e.getMessage());
            return false;
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    logger.warn("SQLException caught trying to close the connection in the testConnection method: " +
                            e.getMessage());
                }
            }
        }

        try {
            connection = DriverManager.getConnection(this.varsConnectionUrl);
        } catch (SQLException e) {
            logger.error("SQLException caught on connection: " + e.getMessage());
            return false;
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    logger.warn("SQLException caught trying to close the connection in the testConnection method: " +
                            e.getMessage());
                }
            }
        }

        // TODO kgomes, can I test MongoDB connection?

        // Return that all was OK
        return true;
    }

    /**
     * This is the method that kicks off the extraction process
     */
    public void extractData() {
        // First grab all the ROV dives that we are interested in and put in the CSV file
        //this.getROVStartAndEndDates();

        // Then get all the annotations using that file for input
        //this.getAnnotations();

        // Now remove and possible duplicates
        //this.cleanDuplicateAnnotations();

        // And now push the data to the MongoDB collection
        this.pushToRepository();
    }

    /**
     * This method queries the Midwater database for ROV dives and then writes them to a results file and returns the
     * number of records that were found
     *
     * @return
     */
    private Integer getROVStartAndEndDates() {

        logger.debug("Going to get ROV start and end dates");
        // The number of rows found in the database
        int numRows = 0;

        // Grab a connection
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(this.midwaterConnectionUrl);
        } catch (SQLException e) {
            logger.error("SQLException caught trying to get connection: " + e.getMessage());
        }

        // Run the query on the database if a connection was made
        if (connection != null) {
            // Create a statement
            Statement statement = null;
            try {
                statement = connection.createStatement();
            } catch (SQLException e) {
                logger.error("SQLException caught trying to create a statement: " + e.getMessage());
            }

            // If the statement was created OK, run the query
            ResultSet rs = null;
            if (statement != null) {
                try {
                    rs = statement.executeQuery("SELECT id, dive, rov, day_night, year, start_datetime_gmt, " +
                            "end_datetime_gmt, depth_m, volume_m3 FROM transect_data INNER JOIN (SELECT rov as 'rov2', " +
                            "dive as 'dive2', count(*) as 'numRows' FROM transect_data WHERE day_night = 'D' " +
                            "AND year >= 1997 GROUP BY rov, dive HAVING count(*) >= 3) table2 " +
                            "ON transect_data.rov=table2.rov2 AND transect_data.dive = table2.dive2 " +
                            "WHERE depth_m in (50,100,200,300,400,500,600,700,800,900,1000) " +
                            "ORDER BY depth_m, start_datetime_gmt");

                } catch (SQLException e) {
                    logger.error("SQLException caught trying to run the query: " + e.getMessage());
                }
            }

            // Loop over the rows and build the result
            if (rs != null) {
                // If a results file was provided, create a writer and write the results
                FileWriter rovStartEndDateFileWriter = null;
                try {
                    rovStartEndDateFileWriter = new FileWriter(midwaterExtractionFile);
                    rovStartEndDateFileWriter.write("Transect Target Depth (m), Transect Start Date (GMT), " +
                            "Transect End Date (GMT), Year, ROV, Dive, Day_Night, Transect Volume (m^3)\n");
                    while (rs.next()) {
                        numRows++;
                        rovStartEndDateFileWriter.write(rs.getFloat("depth_m") + "," +
                                rs.getTimestamp("start_datetime_gmt") + "," +
                                rs.getTimestamp("end_datetime_gmt") + "," +
                                rs.getInt("year") + "," +
                                rs.getString("rov") + "," +
                                rs.getInt("dive") + "," +
                                rs.getString("day_night") + "," +
                                rs.getFloat("volume_m3") + "\n");
                    }
                    rovStartEndDateFileWriter.close();
                    logger.info(numRows + " ROV Dives found");
                } catch (SQLException e) {
                    logger.error("SQLException caught looping over the results of the query: " + e.getMessage());
                } catch (IOException e) {
                    logger.error("IOException caught trying to write rov start and end dates to file: " +
                            e.getMessage());
                }
            }

            // Try to close the result set
            if (rs != null) try {
                rs.close();
            } catch (SQLException e) {
                logger.warn("SQLException caught trying to close the resultset after query failed: " +
                        e.getMessage());
            }
            // Try to close the statement
            if (statement != null) try {
                statement.close();
            } catch (SQLException e) {
                logger.warn("SQLException caught trying to close the statement after query failed: " +
                        e.getMessage());
            }
            // Try to close the connection
            if (connection != null) try {
                connection.close();
            } catch (SQLException e) {
                logger.warn("SQLException caught trying to close the connection after query failed: " +
                        e.getMessage());
            }
        }
        // Return the number of rows
        logger.debug("End search for rov start and end dates and found " + numRows);
        return numRows;
    }

    /**
     * This is the method to grab the annotations associated with the ROV start and end dates given
     * in the ROV results file
     */
    public Integer getAnnotations() {

        logger.debug("Going to get annotations");
        // The number of annotations found
        int numberOfAnnotations = 0;

        // Create writers for the two results files
        FileWriter varsAnnotationsWriter = null;
        FileWriter possibleDuplicateAnnotationsWriter = null;
        try {
            varsAnnotationsWriter = new FileWriter(varsAnnotationsFile);
            // Write the header
            varsAnnotationsWriter.write(
                    "Transect Target Depth (m), " +
                            "Transect Start Date (GMT), " +
                            "Transect End Date (GMT), " +
                            "Year, " +
                            "ROV, " +
                            "Ship," +
                            "Dive, " +
                            "Chief Scientist, " +
                            "Day_Night, " +
                            "Transect Volume (m^3), " +
                            "Recorded Date, " +
                            "Latitude, " +
                            "Longitude, " +
                            "Depth (m), " +
                            "Observation Date (GMT), " +
                            "Observer, " +
                            "Concept Name, " +
                            "Count, " +
                            "ObservationID_FK, " +
                            "Link Name, " +
                            "To Concept, " +
                            "Link Value, " +
                            "Associations, " +
                            "Video Archive Name\n");

            // If a duplicates file was specified, open a writer and write the header
            if (possibleDuplicateAnnotationsFile != null) {
                possibleDuplicateAnnotationsWriter = new FileWriter(possibleDuplicateAnnotationsFile);
                possibleDuplicateAnnotationsWriter.write("ConceptName,VideoArchiveName,LinkValue,HashCode ID\n");
            }

        } catch (IOException e) {
            logger.error("IOException caught trying to open files to write results to: " + e.getMessage());
        }

        // Make sure the ROV results file exists
        if (midwaterExtractionFile != null && midwaterExtractionFile.exists()) {

            // Create an array of strings that will be the concepts to search for and any children under those concepts
//            ArrayList<String> allConceptArrayList = new ArrayList<String>();
//            TreeMap<String, String[]> conceptLookup = new TreeMap<String, String[]>();
//
//            // For each concept listed in the configuration file, we need to query to see if it has any children that
//            // need to be included in the query.  Make sure there is something in the concept list
//            if (conceptList != null && conceptList.length > 0 && !conceptList[0].equals("*")) {
//                for (String concept : conceptList) {
//                    logger.debug("Going to look for children of the concept " + concept);
//                    // Create the Url
//                    String conceptUrlString = this.phylogenyUrlBase + "down/" + concept;
//                    URL conceptUrl = null;
//                    try {
//                        conceptUrl = new URL(conceptUrlString);
//                    } catch (MalformedURLException e) {
//                        logger.error("MalformedURLException caught trying to create URL from " + conceptUrlString +
//                                ": " + e.getMessage());
//                    }
//                    // Check to make sure we have a URL
//                    if (conceptUrl != null) {
//                        // Create the connection
//                        URLConnection urlConnection = null;
//                        try {
//                            urlConnection = conceptUrl.openConnection();
//                        } catch (IOException e) {
//                            logger.error("IOException caught trying to connect to URL " + conceptUrlString +
//                                    ": " + e.getMessage());
//                        }
//
//                        // Make sure we connected OK
//                        if (urlConnection != null) {
//                            // Create a buffered reader
//                            BufferedReader in = null;
//                            try {
//                                in = new BufferedReader(
//                                        new InputStreamReader(
//                                                urlConnection.getInputStream()));
//                            } catch (IOException e) {
//                                logger.error("IOException caught trying to get input stream from URL " +
//                                        conceptUrlString + ": " + e.getMessage());
//                            }
//
//                            // Make sure we have an input stream
//                            if (in != null) {
//                                // Create a string builder to get the response
//                                StringBuilder response = new StringBuilder();
//
//                                // Now read each line and append to the response
//                                String inputLine;
//
//                                try {
//                                    while ((inputLine = in.readLine()) != null)
//                                        response.append(inputLine);
//                                } catch (IOException e) {
//                                    logger.error("IOException caught trying to read lines from " + conceptUrlString +
//                                            ": " + e.getMessage());
//                                }
//
//                                try {
//                                    in.close();
//                                } catch (IOException e) {
//                                    logger.error("IOException caught trying to close the input stream: " + e.getMessage());
//                                }
//
//                                // Convert the JSON string to an object
//                                JSONParser jsonParser = new JSONParser();
//
//                                JSONObject phylogenyTree = null;
//                                try {
//                                    phylogenyTree = (JSONObject) jsonParser.parse(response.toString());
//                                } catch (org.json.simple.parser.ParseException e) {
//                                    logger.error("ParseException trying to parse the JSON string " + response.toString());
//                                }
//
//                                // Check to see if we successfully created the JSON object
//                                if (phylogenyTree != null) {
//                                    logger.debug("Phylogeny tree " + phylogenyTree.keySet());
//                                    parsePhylogenyChildren(phylogenyTree,
//                                            phylogenyTree.get("name").toString().toLowerCase().replace(" ", "_"), "",
//                                            allConceptArrayList, conceptLookup);
//                                }
//                            }
//                        }
//                    }
//                }
//            }

            // Open the ROV results file for reading
            BufferedReader rovResultsFileReader = null;
            try {
                rovResultsFileReader = new BufferedReader(new FileReader(midwaterExtractionFile));
            } catch (FileNotFoundException e) {
                logger.error("FileNotFoundException caught trying to open ROV results file for reading: " +
                        e.getMessage());
            }

            // Make sure the reader was created
            if (rovResultsFileReader != null) {
                // Grab a connection to the VARS database
                Connection connection = null;
                try {
                    connection = DriverManager.getConnection(this.varsConnectionUrl);
                } catch (SQLException e) {
                    logger.error("SQLException caught trying to get connection: " + e.getMessage());
                }

                // Run the query on the database if a connection was made
                if (connection != null) {
                    // The beginning text for the prepared statement
                    String preparedStatementString = "SELECT ObservationDate, Observer, ConceptName, RecordedDate, " +
                            "videoArchiveName, ShipName, RovName, DiveNumber, ChiefScientist, DEPTH, Latitude, " +
                            "Longitude, ObservationID_FK, LinkName, ToConcept, LinkValue, Associations " +
                            "FROM Annotations WHERE RovName = ? AND RecordedDate >= ? AND RecordedDate <= ? ";

                    // Check to see if the query is for all concepts
                    if (conceptList != null && conceptList.length > 0 &&
                            !conceptList[0].equals("*")) {
                        // If there is a concept list specified, add it to the query
                        preparedStatementString += "AND (";
                        int conceptNumber = 0;
                        for (String concept : conceptList) {
                            if (conceptNumber > 0) preparedStatementString += " OR ";
                            preparedStatementString += "ConceptName = '" + concept + "'";
                            conceptNumber++;
                        }
                        preparedStatementString += ") ";
                    }

                    // Add th order by clause
                    preparedStatementString += "ORDER BY RecordedDate ASC";
                    logger.info("Query template is:");
                    logger.info(preparedStatementString);

                    // Create a prepared statement
                    PreparedStatement statement = null;

                    try {
                        statement = connection.prepareStatement(preparedStatementString);
                    } catch (SQLException e) {
                        logger.error("SQLException caught trying to create prepared statement: " + e.getMessage());
                    }

                    // Make sure the statement is there
                    if (statement != null) {
                        try {
                            // Read off the first line (the header)
                            String nextLine = rovResultsFileReader.readLine();
                            // Now start reading line-by-line
                            while ((nextLine = rovResultsFileReader.readLine()) != null) {
                                // Parse the values from the ROV line
                                String[] rovStartEnds = nextLine.split(",");

                                String depthString = rovStartEnds[0].trim();
                                String startDateGmt = rovStartEnds[1].trim();
                                String endDateGmt = rovStartEnds[2].trim();
                                String year = rovStartEnds[3].trim();
                                String rov = rovStartEnds[4].trim();
                                String dive = rovStartEnds[5].trim();
                                String day_night = rovStartEnds[6].trim();
                                String volume = rovStartEnds[7].trim();

                                // Create a default count as 1 (each annotation = one concept)
                                int conceptCount = 1;

                                // Build the start and end dates
                                java.util.Date startDate = null;
                                java.util.Date endDate = null;
                                try {
                                    startDate = simpleDateFormat.parse(startDateGmt);
                                    endDate = simpleDateFormat.parse(endDateGmt);
                                } catch (Exception e) {
                                    logger.error("Exception caught trying to parse string to SQL dates: " +
                                            e.getMessage());
                                }
                                // And the depth
                                Float depth = null;
                                try {
                                    depth = Float.parseFloat(depthString);
                                } catch (NumberFormatException e) {
                                    logger.error("NumberFormatException caught trying to parse the depth " +
                                            "from the ROV results file: " + e.getMessage());
                                }

                                // Fill in the query
                                if (rov != null && startDate != null && endDate != null) {
                                    try {
                                        statement.setString(1, rov);
                                        statement.setTimestamp(2, new Timestamp(startDate.getTime()));
                                        statement.setTimestamp(3, new Timestamp(endDate.getTime()));
                                    } catch (SQLException e) {
                                        logger.error("SQLException caught trying to fill in the values in the query: " + e.getMessage());
                                    }
                                }

                                // If the statement was created OK, run the query
                                ResultSet rs = null;
                                try {
                                    rs = statement.executeQuery();
                                } catch (SQLException e) {
                                    logger.error("SQLException caught trying to run the query: " + e.getMessage());
                                }

                                // Loop over the rows and build the result
                                if (rs != null) {
                                    try {
                                        while (rs.next()) {

                                            // Make sure the depth is within 40m of expected ROV depth
                                            if (rs.getFloat("DEPTH") != Float.NaN &&
                                                    rs.getFloat("DEPTH") > (depth - 40) &&
                                                    rs.getFloat("DEPTH") < (depth + 40)) {
                                                // Check to see if link name is identity-reference which could make it a
                                                // candidate for duplicate cleaning
                                                if (possibleDuplicateAnnotationsWriter != null) {
                                                    if (rs.getString("LinkName") != null &&
                                                            rs.getString("LinkName").equals("identity-reference")) {
                                                        // Create hash of ConceptName, LinkValue and VideoArchiveName
                                                        String hashString = rs.getString("ConceptName") + "-" +
                                                                rs.getString("LinkValue") +
                                                                "-" + rs.getString("VideoArchiveName");
                                                        possibleDuplicateAnnotationsWriter.write(rs.getString("ConceptName") +
                                                                "," + rs.getString("VideoArchiveName") + "," +
                                                                rs.getString("LinkValue") + "," + hashString.hashCode() + "\n");
                                                    }
                                                }

                                                // Check to see if the link name is 'population-quantity' as that indicates
                                                // a new count number
                                                if (rs.getString("LinkName") != null &&
                                                        rs.getString("LinkName").equals("population-quantity") &&
                                                        rs.getString("LinkValue") != null) {
                                                    try {
                                                        conceptCount = Integer.parseInt(rs.getString("LinkValue"));
                                                    } catch (NumberFormatException e) {
                                                        logger.error("NumberFormatException trying to convert link " +
                                                                "value of " + rs.getString("LinkValue") + " to a number");
                                                    } catch (SQLException e) {
                                                        logger.error("SQLException trying to convert link " +
                                                                "value of " + rs.getString("LinkValue") + " to a number");
                                                    }
                                                }


                                                // Write the raw annotation to the file
                                                if (varsAnnotationsWriter != null) {
                                                    numberOfAnnotations++;
                                                    // If the number of counts was 999, we override that to a value of
                                                    // 50 which is more reasonable, per a recommendation from Rob and
                                                    // Monique
                                                    int conceptCountToWrite = conceptCount;
                                                    if (conceptCountToWrite == 999) {
                                                        conceptCountToWrite = 50;
                                                    }
//                                                    // First is convert the concept name back to it's original so we
//                                                    // can query for phylogeny tree later
//                                                    String originalConceptName = rs.getString("ConceptName").replaceAll("_", " ");
//                                                    originalConceptName =
//                                                            originalConceptName.substring(0, 1).toUpperCase() +
//                                                                    originalConceptName.substring(1);
                                                    try {
                                                        varsAnnotationsWriter.write(
                                                                depth + "," +
                                                                        startDateGmt + "," +
                                                                        endDateGmt + "," +
                                                                        year + "," +
                                                                        "\"" + rov + "\"," +
                                                                        "\"" + rs.getString("ShipName") + "\"," +
                                                                        dive + "," +
                                                                        "\"" + rs.getString("ChiefScientist") + "\"," +
                                                                        day_night + "," +
                                                                        volume + "," +
                                                                        rs.getTimestamp("RecordedDate") + "," +
                                                                        rs.getFloat("Latitude") + "," +
                                                                        rs.getFloat("Longitude") + "," +
                                                                        rs.getFloat("DEPTH") + "," +
                                                                        rs.getTimestamp("ObservationDate") + "," +
                                                                        "\"" + rs.getString("Observer") + "\"," +
                                                                        rs.getString("ConceptName").toLowerCase().replace(" ", "_") + "," +
                                                                        conceptCountToWrite + "," +
                                                                        rs.getLong("ObservationID_FK") + "," +
                                                                        "\"" + rs.getString("LinkName") + "\"," +
                                                                        "\"" + rs.getString("ToConcept") + "\"," +
                                                                        "\"" + rs.getString("LinkValue") + "\"," +
                                                                        "\"" + rs.getString("Associations") + "\"," +
                                                                        "\"" + rs.getString("VideoArchiveName") + "\"   \n");
                                                    } catch (IOException e) {
                                                        logger.error("IOException caught trying to write raw annotation to " +
                                                                " data file: " + e.getMessage());
                                                        e.printStackTrace();
                                                    }
                                                }
                                            }
                                        }
                                    } catch (SQLException e) {
                                        logger.error("SQLException caught looping over the results of the query: " +
                                                e.getMessage());
                                    }
                                }

                                // Try to close the result set
                                if (rs != null) try {
                                    rs.close();
                                } catch (SQLException e) {
                                    logger.warn("SQLException caught trying to close the resultset after query failed: " +
                                            e.getMessage());
                                }

                            }
                        } catch (IOException e) {
                            logger.error("IOException caught trying to read ROV results file: " + e.getMessage());
                        }

                    }
                    // Try to close the statement
                    if (statement != null) try {
                        statement.close();
                    } catch (SQLException e) {
                        logger.warn("SQLException caught trying to close the statement after query failed: " +
                                e.getMessage());
                    }
                    // Try to close the connection
                    if (connection != null) try {
                        connection.close();
                    } catch (SQLException e) {
                        logger.warn("SQLException caught trying to close the connection after query failed: " +
                                e.getMessage());
                    }

                    // Close the data file if it exists
                    if (varsAnnotationsWriter != null) {
                        try {
                            varsAnnotationsWriter.close();
                        } catch (IOException e) {
                            logger.error("IOException caught trying to close data file writer: " + e.getMessage());
                        }
                    }

                    // Close the duplicate list file
                    if (possibleDuplicateAnnotationsWriter != null) {
                        try {
                            possibleDuplicateAnnotationsWriter.close();
                        } catch (IOException e) {
                            logger.error("IOException caught trying to close possible duplicates file writer: " +
                                    e.getMessage());
                        }
                    }
                }
            }
        }

        // Return the number of annotations found
        logger.debug("Done getting annotations and found " + numberOfAnnotations);
        return numberOfAnnotations;
    }


//    private void parsePhylogenyChildren(JSONObject conceptObject, String topConcept, String parentTree,
//                                        ArrayList<String> allConceptArrayList,
//                                        TreeMap<String, String[]> conceptLookup) {
//        // First pull out the name and rank
//        String conceptName = conceptObject.get("name").toString().toLowerCase().replace(" ", "_");
//        String conceptRank = conceptObject.get("rank").toString();
//
//        // And the children array
//        JSONArray childrenArray = (JSONArray) conceptObject.get("children");
//
//        // First add the concept name to the concept list
//        allConceptArrayList.add(conceptName);
//
//        // Create the new parent tree that includes this concept rank and name
//        String parentTreeString = parentTree;
//        if (!parentTree.equals("")) parentTreeString += ";";
//        parentTreeString += conceptRank + ":" + conceptName;
//
//        // Now create the array of top level concept and parent tree for the lookup tree
//        String[] lookUpArray = new String[]{topConcept, parentTreeString};
//        conceptLookup.put(conceptName, lookUpArray);
//
//        // Now recursivley deal with the children
//        if (((JSONArray) conceptObject.get("children")).size() > 0) {
//            for (Object childConceptObject : (JSONArray) conceptObject.get("children")) {
//                parsePhylogenyChildren((JSONObject) childConceptObject, topConcept, parentTreeString,
//                        allConceptArrayList, conceptLookup);
//            }
//        }
//        return;
//    }

    /**
     * This is the method to clean any duplicates out of the annotations extracted from the database
     */
    private Integer cleanDuplicateAnnotations() {
        logger.debug("Going to clean duplicate annotations");

        // This is the number of annotations that were cleaned out
        int numberOfAnnotationsRemoved = 0;

        // This is the HashMap for the duplicate hashcode to how many matches were found
        HashMap<Integer, Integer> hashCodeMap = new HashMap<Integer, Integer>();

        // This is a TreeMap that contains the mapping of ObservationID_FK to ConceptName so we can look for duplicates
        TreeMap<Long, String> observationID_FKs = new TreeMap<>();

        // Open the duplicates file for reading
        BufferedReader possibleDuplicateAnnotationsFileReader = null;
        try {
            possibleDuplicateAnnotationsFileReader =
                    new BufferedReader(new FileReader(this.possibleDuplicateAnnotationsFile));
        } catch (FileNotFoundException e) {
            logger.error("FileNotFoundException caught trying to open possible duplicate annotations file for reading: " +
                    e.getMessage());
        }

        // If the reader is OK, read in hash codes into a map
        try {
            // Read the header line first
            String nextLine = possibleDuplicateAnnotationsFileReader.readLine();
            // Now loop through and read all the lines in the file
            while ((nextLine = possibleDuplicateAnnotationsFileReader.readLine()) != null) {
                // Grab the hashcode
                Integer hashCode = Integer.parseInt(nextLine.split(",")[3]);

                // See if it's in the HashMap and put it in there if not
                if (!hashCodeMap.containsKey(hashCode)) {
                    hashCodeMap.put(hashCode, 0);
                }
            }
        } catch (IOException e) {
            logger.error("IOException caught trying to read in duplicate hash codes: " + e.getMessage());
        }

        // Close the reader
        if (possibleDuplicateAnnotationsFileReader != null) {
            try {
                possibleDuplicateAnnotationsFileReader.close();
            } catch (IOException e) {
                logger.error("IOException trying to close the possible duplicate file reader: " + e.getMessage());
            }
        }

        // Open the source file for reading
        BufferedReader varsAnnotationsFileReader = null;
        try {
            varsAnnotationsFileReader = new BufferedReader(new FileReader(this.varsAnnotationsFile));
        } catch (FileNotFoundException e) {
            logger.error("FileNotFoundException caught trying to open vars annotations file for reading: " +
                    e.getMessage());
        }

        // Open the cleaned file for writing
        FileWriter varsAnnotationsDuplicatesRemovedWriter = null;
        try {
            varsAnnotationsDuplicatesRemovedWriter = new FileWriter(this.varsAnnotationsFileDuplicatesRemoved);
        } catch (IOException e) {
            logger.error("IOException caught trying to open the cleaned annotation file for writing: " + e.getMessage());
        }

        // Make sure readers and writers are ready to go
        if (varsAnnotationsFileReader != null && varsAnnotationsDuplicatesRemovedWriter != null) {
            // Read the header first
            try {
                String nextLine = varsAnnotationsFileReader.readLine();
                // Write it to the clean
                varsAnnotationsDuplicatesRemovedWriter.write(nextLine + "\n");
                // Now loop over the rest of the file
                int lineCounter = 0;
                while ((nextLine = varsAnnotationsFileReader.readLine()) != null) {
                    lineCounter++;

                    // Split up the line
                    String[] lineCols = nextLine.split(",");

                    // Grab the ObservationID_FK
                    String observationID_FK = lineCols[18].trim();
                    // Convert to a long
                    Long observationID_FK_Long = null;
                    try {
                        observationID_FK_Long = Long.parseLong(observationID_FK);
                    } catch (NumberFormatException e) {
                        logger.error("NumberFormatException caught trying to parse observationID_FK: " + e.getMessage());
                    }

                    // Grab the link name
                    String linkName = lineCols[19].trim();

                    // Grab the ConceptName
                    String conceptName = lineCols[16].trim();

                    // Grab the LinkValue
                    String linkValue = lineCols[21].trim();

                    // Grab the video archive name
                    String videoArchiveName = lineCols[23].trim();

                    // Calculate the hashcode for the line using ConceptName, LinkValue, and VideoArchiveName
                    Integer lineHashCode = (conceptName + "-" + linkValue + "-" + videoArchiveName).hashCode();

                    // Check to see if the hash code is in the map of duplicates
                    if (linkName.equals("identity-reference") && hashCodeMap.containsKey(lineHashCode)) {
                        // This means it is a possible duplicate so check to see if a line has already been written
                        // (count > zero) or if the link name is 'population-quantity'
                        if (hashCodeMap.get(lineHashCode) == 0 || lineCols[19].trim().equals("population-quantity")) {
                            // It has not been encountered before, so write it out
                            try {
                                varsAnnotationsDuplicatesRemovedWriter.write(nextLine + "\n");
                            } catch (IOException e) {
                                logger.error("IOException writing line " + lineCounter);
                            }
                        } else {
                            // The line was skipped, bump the counter
                            numberOfAnnotationsRemoved++;
                            logger.debug("Removed line: " + lineCounter + "->" + nextLine);
                        }

                        // Bump the counter
                        hashCodeMap.put(lineHashCode, hashCodeMap.get(lineHashCode) + 1);
                    } else {
//                        // We still need to check and see if the obs ID has already been seen
//                        if (!observationID_FKs.containsKey(observationID_FK_Long)) {
//                            observationID_FKs.put(observationID_FK_Long, conceptName);
//
//                            // This is not a possible duplicate and so should be written to the clean file
//                            try {
//                                varsAnnotationsDuplicatesRemovedWriter.write(nextLine + "\n");
//                            } catch (IOException e) {
//                                logger.error("IOException caught trying to write line " + lineCounter);
//                            }
//                        } else {
//                            // Check to see if the both concept names are the same and have the same obs ID
//                            if (conceptName.equals(observationID_FKs.get(observationID_FK_Long))) {
//                                // The line was skipped, bump the counter
//                                numberOfAnnotationsRemoved++;
//                                logger.debug("Concept name and observation ID have been seen before, removing line " +
//                                        lineCounter + "->" + nextLine);
//                            } else {
                        try {
                            varsAnnotationsDuplicatesRemovedWriter.write(nextLine + "\n");
                        } catch (IOException e) {
                            logger.error("IOException caught trying to write line " + lineCounter);
                        }
//                            }
//                        }
                    }
                }
            } catch (IOException e) {
                logger.error("IOException caught trying to read lines from annotations file: " + e.getMessage());
            }

            // Now close the files
            try {
                if (varsAnnotationsFileReader != null) {
                    varsAnnotationsFileReader.close();
                }
                if (varsAnnotationsDuplicatesRemovedWriter != null) {
                    varsAnnotationsDuplicatesRemovedWriter.close();
                }
            } catch (IOException e) {
                logger.error("IOException caught trying to close the dirty and clean annotation files: " +
                        e.getMessage());
            }
        }

        // Return the number of annotations that were removed
        logger.debug("Removed " + numberOfAnnotationsRemoved + " annotations as duplicates");
        return numberOfAnnotationsRemoved;
    }

    /**
     * This method takes the data from the midwater database (with duplicates removed) and loads them
     * in the Mongo database
     */
    private void pushToRepository() {

        logger.debug("Staring the push to the MongoDB repo");
        // If the VARS_KB connection URL is defined, make a connection
        Connection varsKBConnection = null;
        PreparedStatement phylogenyUpStatement = null;
        if (this.varsKBConnectionUrl != null) {
//            try {
//                varsKBConnection = DriverManager.getConnection(this.varsKBConnectionUrl);
//            } catch (SQLException e) {
//                logger.error("SQLException caught trying to get connection: " + e.getMessage());
//            }

            // If the connection worked, create a prepared statement
            if (varsKBConnection != null) {

                String phylogenyUpQuery = "WITH org_name AS\n" +
                        "    (\n" +
                        "        SELECT DISTINCT\n" +
                        "            parent.id AS parent_id,\n" +
                        "            parentname.ConceptName as parent_name,\n" +
                        "            ISNULL(parent.RankLevel, '') + ISNULL(parent.RankName, '') AS parent_rank,\n" +
                        "            child.id AS child_id,\n" +
                        "            childname.ConceptName as child_name,\n" +
                        "            ISNULL(child.RankLevel, '') + ISNULL(child.RankName, '') AS child_rank\n" +
                        "        FROM\n" +
                        "            Concept parent RIGHT OUTER JOIN\n" +
                        "            Concept child ON child.ParentConceptID_FK = parent.id LEFT OUTER JOIN\n" +
                        "            ConceptName childname ON childname.ConceptID_FK = child.id LEFT OUTER JOIN\n" +
                        "            ConceptName parentname ON parentname.ConceptID_FK = parent.id\n" +
                        "        WHERE\n" +
                        "            childname.NameType = 'Primary' AND\n" +
                        "            parentname.NameType = 'Primary'\n" +
                        "    ),\n" +
                        "    jn AS\n" +
                        "    (\n" +
                        "        SELECT\n" +
                        "            parent_id,\n" +
                        "            parent_name,\n" +
                        "            parent_rank,\n" +
                        "            child_id,\n" +
                        "            child_name,\n" +
                        "            child_rank\n" +
                        "        FROM\n" +
                        "            org_name\n" +
                        "        WHERE\n" +
                        "            child_name = ?\n" +
                        "        UNION ALL\n" +
                        "            SELECT\n" +
                        "                C.parent_id,\n" +
                        "                C.parent_name,\n" +
                        "                C.parent_rank,\n" +
                        "                C.child_id,\n" +
                        "                C.child_name,\n" +
                        "                C.child_rank\n" +
                        "            FROM\n" +
                        "                jn AS p JOIN\n" +
                        "                org_name AS C ON C.child_id = p.parent_id\n" +
                        "    )\n" +
                        "SELECT DISTINCT\n" +
                        "    jn.parent_id,\n" +
                        "    jn.parent_name,\n" +
                        "    jn.parent_rank,\n" +
                        "    jn.child_id,\n" +
                        "    jn.child_name,\n" +
                        "    jn.child_rank\n" +
                        "FROM\n" +
                        "    jn\n" +
                        "ORDER BY\n" +
                        "    1";
                try {
                    phylogenyUpStatement = varsKBConnection.prepareStatement(phylogenyUpQuery);
                } catch (SQLException e) {
                    logger.error("SQLException creating prepared statement for phylogeny up: " + e.getMessage());
                }
            }

        }

        // Drop the existing collection
        mongoCollection.drop();

        // Let's create a Map that we can use to store pylogeny info so we can cut down on the number of
        // requests
        Map<String, JSONObject> phyloMap = new TreeMap<String, JSONObject>();

        // The list of Documents to insert
        List<Document> documents = new ArrayList<Document>();

        // Make sure the clean annotation file exists
        if (this.varsAnnotationsFileDuplicatesRemoved != null &&
                this.varsAnnotationsFileDuplicatesRemoved.exists()) {
            // Now, open the cleaned file for reading
            BufferedReader varsAnnotationsFileDuplicatesRemovedReader = null;
            try {
                varsAnnotationsFileDuplicatesRemovedReader =
                        new BufferedReader(new FileReader(this.varsAnnotationsFileDuplicatesRemoved));
            } catch (FileNotFoundException e) {
                logger.error("FileNotFoundException caught trying to open the clean annoations file for reading: " +
                        e.getMessage());
            }

            try {
                // Read in the header line
                String nextLine = varsAnnotationsFileDuplicatesRemovedReader.readLine();
                // Save it for later
                String headerLine = nextLine;

                // A line counter
                int lineCounter = 1;

                // Now read in the rest of the file line by line
                while ((nextLine = varsAnnotationsFileDuplicatesRemovedReader.readLine()) != null) {

                    // Split the line up
                    String[] splitLine = nextLine.split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)", -1);

                    // The target depth
                    String targetDepthString = splitLine[0];
                    // Convert to a number
                    Float targetDepth = null;
                    try {
                        targetDepth = Float.parseFloat(targetDepthString);
                    } catch (NumberFormatException e) {
                        logger.error("NumberFormatException caught trying to conver target depth string of " +
                                targetDepthString + " to a float: " + e.getMessage());
                        logger.error("Line " + lineCounter + "->" + nextLine);
                        logger.error(splitLine);
                    }

                    // Transect start date
                    String transectStartDateString = splitLine[1];
                    java.util.Date transectStartDate = null;
                    try {
                        transectStartDate = midwaterDateFormat.parse(transectStartDateString);
                    } catch (ParseException e) {
                        logger.error("ParseException for transect start date of " + transectStartDateString +
                                ": " + e.getMessage());
                        logger.error("Line " + lineCounter + "->" + nextLine);
                        logger.error(splitLine);
                    }

                    // Transect end date
                    String transectEndDateString = splitLine[2];
                    java.util.Date transectEndDate = null;
                    try {
                        transectEndDate = midwaterDateFormat.parse(transectEndDateString);
                    } catch (ParseException e) {
                        logger.error("ParseException for transect end date of " + transectEndDateString +
                                ": " + e.getMessage());
                        logger.error("Line " + lineCounter + "->" + nextLine);
                        logger.error(splitLine);
                    }

                    // Year
                    String yearString = splitLine[3];
                    Integer year = null;
                    try {
                        year = Integer.parseInt(yearString);
                    } catch (NumberFormatException e) {
                        logger.error("NumberFormatException caught trying to convert year string of " +
                                yearString + ": " + e.getMessage());
                        logger.error("Line " + lineCounter + "->" + nextLine);
                        logger.error(splitLine);
                    }

                    // ROV
                    String rovString = splitLine[4];

                    // Ship
                    String shipString = splitLine[5];

                    // Dive
                    String diveNumberString = splitLine[6];
                    Integer diveNumber = null;
                    try {
                        diveNumber = Integer.parseInt(diveNumberString);
                    } catch (NumberFormatException e) {
                        logger.error("NumberFormatException caught trying to convert dive number string of " +
                                diveNumberString + ": " + e.getMessage());
                        logger.error("Line " + lineCounter + "->" + nextLine);
                        logger.error(splitLine);
                    }

                    // Chief Scientist
                    String chiefScientistString = splitLine[7];

                    // Day_night
                    String dayNightString = splitLine[8];

                    // The volume
                    String volumeString = splitLine[9];
                    Float volume = null;
                    try {
                        volume = Float.parseFloat(volumeString);
                    } catch (NumberFormatException e) {
                        logger.error("NumberFormatException trying to convert volume string of " +
                                volumeString + ": " + e.getMessage());
                        logger.error("Line " + lineCounter + "->" + nextLine);
                        logger.error(splitLine);
                    }

                    // The recorded date
                    String recordedDateString = splitLine[10];
                    java.util.Date recordedDate = null;
                    try {
                        recordedDate = midwaterDateFormat.parse(recordedDateString);
                    } catch (ParseException e) {
                        logger.error("ParseException for recorded date string of " + recordedDateString +
                                ": " + e.getMessage());
                        logger.error("Line " + lineCounter + "->" + nextLine);
                        logger.error(splitLine);
                    }

                    // The latitude
                    String latitudeString = splitLine[11];
                    Float latitude = null;
                    try {
                        latitude = Float.parseFloat(latitudeString);
                    } catch (NumberFormatException e) {
                        logger.error("NumberFormatException trying to convert latitude string of " + latitudeString +
                                ": " + e.getMessage());
                        logger.error("Line " + lineCounter + "->" + nextLine);
                        logger.error(splitLine);
                    }

                    // The longitude
                    String longitudeString = splitLine[12];
                    Float longitude = null;
                    try {
                        longitude = Float.parseFloat(longitudeString);
                    } catch (NumberFormatException e) {
                        logger.error("NumberFormateException trying to convert longitude string of " +
                                longitudeString + ": " + e.getMessage());
                        logger.error("Line " + lineCounter + "->" + nextLine);
                        logger.error(splitLine);
                    }

                    // Actual depth
                    String actualDepthString = splitLine[13];
                    Float actualDepth = null;
                    try {
                        actualDepth = Float.parseFloat(actualDepthString);
                    } catch (NumberFormatException e) {
                        logger.error("NumberFormatException trying to convert acutal depth string of " +
                                actualDepthString + ": " + e.getMessage());
                        logger.error("Line " + lineCounter + "->" + nextLine);
                        logger.error(splitLine);
                    }

                    // Observation date
                    String observationDateString = splitLine[14];
                    java.util.Date observationDate = null;
                    try {
                        observationDate = midwaterDateFormat.parse(observationDateString);
                    } catch (ParseException e) {
                        logger.error("ParseException for observation date of " + observationDateString +
                                ": " + e.getMessage());
                        logger.error("Line " + lineCounter + "->" + nextLine);
                        logger.error(splitLine);
                    }

                    // Observer
                    String observerString = splitLine[15];

                    // Original concept name
//                    String originalConceptName = splitLine[16];

                    // The concept name (all lower case with spaces replaced by underscores)
                    String conceptNameString = splitLine[16];

                    // Group by concept name
//                    String groupByConceptNameString = splitLine[18];

                    // Concept lineage
//                    String conceptLineageString = splitLine[19];

                    // The count
                    String countString = splitLine[17];
                    Integer count = null;
                    try {
                        count = Integer.parseInt(countString);
                    } catch (NumberFormatException e) {
                        logger.error("NumberFormatException caught trying to convert count string of " + countString +
                                ": " + e.getMessage());
                        logger.error("Line " + lineCounter + "->" + nextLine);
                        logger.error(splitLine);
                    }

                    // ObservationID_FK
                    String observationID_FKString = splitLine[18];

                    // Link name
                    String linkNameString = splitLine[19];

                    // To concept
                    String toConceptString = splitLine[20];

                    // Link value
                    String linkValueString = splitLine[21];

                    // Associations
                    String associationsString = splitLine[22];

                    // Video archive name
                    String videoArchiveString = splitLine[23];

                    // Check to see if depth is greater than zero
                    float depthToRecord = actualDepth;
                    if (depthToRecord > 0) {
                        depthToRecord = -1 * depthToRecord;
                    }

                    // Create a new we can use to query for phylogeny
                    String phylogenyName = conceptNameString.substring(0, 1).toUpperCase() +
                            conceptNameString.replaceAll("_", "%20").substring(1);

                    // The document for the phylogeny
                    Document phylogenyDocument = new Document();

                    // See if we already have a JSONObject for this concept
                    JSONObject phylogenyJSON = phyloMap.get(phylogenyName);

                    // If we found one, use it
                    if (phylogenyJSON == null) {
                        // If the VARS KB statement exists, use that for performance reasons
                        if (phylogenyUpStatement != null) {
                            // Set the concept name
                            try {
                                phylogenyUpStatement.setString(1, conceptNameString);
                                ResultSet phylogenyUpResultSet = phylogenyUpStatement.executeQuery();
                                logger.debug("For concept name: " + conceptNameString);
                                while (phylogenyUpResultSet.next()) {
                                    logger.debug("" + phylogenyUpResultSet.getString(1));
                                }
                                phylogenyUpResultSet.close();
                            } catch (SQLException e) {
                                logger.error("SQLException caught trying to set concept name to " + conceptNameString);
                            }
                            // TODO kgomes - finish implementing this, NOT COMPLETE
                        } else {

                            // Now using the concept name, grab the pylogeny tree above it
                            String phylogenyUrlString = this.phylogenyUrlBase + "up/" + phylogenyName;

                            //logger.debug("phylogeny query->" + phylogenyUrlString);
                            URL phylogenyUrl = null;
                            try {
                                phylogenyUrl = new URL(phylogenyUrlString);
                            } catch (MalformedURLException e) {
                                logger.error("MalformedURLException caught trying to create URL from " + phylogenyUrlString +
                                        ": " + e.getMessage());
                            }
                            // Check to make sure we have a URL
                            if (phylogenyUrl != null) {
                                // Create the connection
                                URLConnection urlConnection = null;
                                try {
                                    urlConnection = phylogenyUrl.openConnection();
                                } catch (IOException e) {
                                    logger.error("IOException caught trying to connect to URL " + phylogenyUrlString +
                                            ": " + e.getMessage());
                                }

                                // Make sure we connected OK
                                if (urlConnection != null) {
                                    // Create a buffered reader
                                    BufferedReader in = null;
                                    try {
                                        in = new BufferedReader(
                                                new InputStreamReader(
                                                        urlConnection.getInputStream()));
                                    } catch (IOException e) {
                                        logger.error("IOException caught trying to get input stream from URL " +
                                                phylogenyUrlString + ": " + e.getMessage());
                                    } catch (Exception e) {
                                        logger.error("Exception caught trying to get input stream from phylogenyUrl: " + e.getMessage());
                                    }

                                    // Make sure we have an input stream
                                    if (in != null) {
                                        // Create a string builder to get the response
                                        StringBuilder response = new StringBuilder();

                                        // Now read each line and append to the response
                                        String inputLine;

                                        try {
                                            while ((inputLine = in.readLine()) != null)
                                                response.append(inputLine);
                                        } catch (IOException e) {
                                            logger.error("IOException caught trying to read lines from " + phylogenyUrlString +
                                                    ": " + e.getMessage());
                                        }

                                        try {
                                            in.close();
                                        } catch (IOException e) {
                                            logger.error("IOException caught trying to close the input stream: " + e.getMessage());
                                        }
                                        //logger.debug("Phylogeny line->" + response);
                                        // Convert the JSON string to an object
                                        JSONParser jsonParser = new JSONParser();

                                        try {
                                            phylogenyJSON = (JSONObject) jsonParser.parse(response.toString());
                                        } catch (org.json.simple.parser.ParseException e) {
                                            logger.error("ParseException trying to parse the JSON string " + response.toString());
                                        }

                                    }
                                }
                            }
                        }
                    }

                    // Now parse the JSONObject into a BSON Document
                    // Check to see if we successfully created the JSON object
                    if (phylogenyJSON != null && phylogenyJSON.get("name") != null &&
                            phylogenyJSON.get("children") != null) {
                        // Add it to the map
                        phyloMap.put(phylogenyName, phylogenyJSON);

                        //logger.debug("jsonObject" + jsonObject);
                        // Start a counter to track what level we are at
                        int levelCounter = 0;
                        boolean done = false;
                        do {
                            //logger.debug("Index " + levelCounter + " " + jsonObject.get("name"));
                            String rank = null;
                            if (levelCounter == 0) {
                                rank = "level-one";
                            } else if (levelCounter == 1) {
                                rank = "level-two";
                            } else if (levelCounter == 2) {
                                rank = "level-three";
                            } else {
                                rank = phylogenyJSON.get("rank").toString();
                            }

                            try {
                                phylogenyDocument.append(rank, phylogenyJSON.get("name").toString());
                            } catch (Exception e) {
                                logger.error("Exception caught: " + e.getMessage());
                                logger.error("Index " + levelCounter + " " + phylogenyJSON.get("name"));
                                throw e;
                            }
                            levelCounter++;
                            // Check to see if there are children
                            if (((JSONArray) phylogenyJSON.get("children")).size() > 0) {
                                phylogenyJSON = (JSONObject) ((JSONArray) phylogenyJSON.get("children")).get(0);
                            } else {
                                done = true;
                            }
                        } while (!done);
                    }



//                    logger.debug("Parsed to: targetDepthString=" + targetDepthString + "->" + targetDepth +
//                            ";transectStartDateString=" + transectStartDateString + "->" + transectStartDate +
//                            ";transectEndDateString=" + transectEndDateString + "->" + transectEndDate +
//                            ";yearString=" + yearString + "->" + year +
//                            ";rovString=" + rovString +
//                            ";shipString=" + shipString +
//                            ";diveNumberString=" + diveNumberString + "->" + diveNumber +
//                            ";chiefScientistString=" + chiefScientistString +
//                            ";dayNightString=" + dayNightString +
//                            ";volumeString=" + volumeString + "->" + volume +
//                            ";recordedDateString=" + recordedDateString + "->" + recordedDate +
//                            ";latitudeString=" + latitudeString + "->" + latitude +
//                            ";longitudeString=" + longitudeString + "->" + longitude +
//                            ";acutalDepthString=" + actualDepthString + "->" + actualDepth +
//                            ";observationDateString=" + observationDateString + "->" + observationDate +
//                            ";observerString=" + observerString +
//                            ";conceptNameString=" + conceptNameString +
//                            ";groupByConceptNameString=" + groupByConceptNameString +
//                            ";conceptLineageString=" + conceptLineageString +
//                            ";countString=" + countString +
//                            ";observationID_FKString=" + observationID_FKString +
//                            ";linkNameString=" + linkNameString +
//                            ";toConceptString=" + toConceptString +
//                            ";linkValueString=" + linkValueString +
//                            ";associationsString=" + associationsString +
//                            ";videoArchiveNameString=" + videoArchiveString);
                    // Create the document
                    Document document = new Document("target-depth", targetDepth)
                            .append("date-resolution", "second")
                            .append("transect-start-date", transectStartDate)
                            .append("transect-end-date", transectEndDate)
                            .append("year", year)
                            .append("rov", rovString.replaceAll("\\\"", ""))
                            .append("ship", shipString.replaceAll("\\\"", ""))
                            .append("dive", diveNumber)
                            .append("chief-scientist", chiefScientistString.replaceAll("\\\"", ""))
                            .append("day-night", dayNightString)
                            .append("transect-volume", volume)
                            .append("recorded-date", recordedDate)
                            .append("loc",
                                    new Document("type", "Point")
                                            .append("coordinates",
                                                    asList(Float.parseFloat(longitudeString),
                                                            Float.parseFloat(latitudeString),
                                                            depthToRecord)))
                            .append("depth", actualDepth)
                            .append("observation-date", observationDate)
                            .append("date", observationDate)
                            .append("observer", observerString.replaceAll("\\\"", ""))
                            .append("name", conceptNameString)
                            .append("phylogeny", phylogenyDocument)
                            .append("value", count)
                            .append("units", "counts")
                            .append("observation-id-fk", observationID_FKString)
                            .append("link-name", (linkNameString.replaceAll("\\\"", "").equals("null")) ? null : linkNameString.replaceAll("\\\"", ""))
                            .append("to-concept", (toConceptString.replaceAll("\\\"", "").equals("null")) ? null : toConceptString.replaceAll("\\\"", ""))
                            .append("link-value", (linkValueString.replaceAll("\\\"", "").equals("null")) ? null : linkValueString.replaceAll("\\\"", ""))
                            .append("associations", (associationsString.replaceAll("\\\"", "").equals("null")) ? null : associationsString.replaceAll("\\\"", ""))
                            .append("video-archive-name", videoArchiveString.replaceAll("\\\"", "").trim());

                    // Add it to the list
                    documents.add(document);

                    // Check the size of the list, if it's over 200, insert them
                    if (documents.size() >= 200){
                        logger.debug("Inserting 200 documents into Mongo");
                        mongoCollection.insertMany(documents);
                        documents = new ArrayList<Document>();
                    }
//                    logger.debug("Adding document for concept " + conceptNameString);
//                    mongoCollection.insertOne(document);

                    // Bump the line counter
                    lineCounter++;
                }
                // Now insert what's left in the list of Mongo documents
                logger.debug("Inserting what's left (" + documents.size() + ")");
                mongoCollection.insertMany(documents);
            } catch (IOException e) {
                logger.error("IOException caught trying to read the clean annotation file: " + e.getMessage());
            }


            // Close the reader
            if (varsAnnotationsFileDuplicatesRemovedReader != null) {
                try {
                    varsAnnotationsFileDuplicatesRemovedReader.close();
                } catch (IOException e) {
                    logger.error("IOException caught trying to close the clean annotations file reader: " + e.getMessage());
                }
            }

            // If we were connected to the VARS KB, close the connection
            if (varsKBConnection != null) {
                try {
                    varsKBConnection.close();
                } catch (SQLException e) {
                    logger.error("SQLException caught trying to close connection to the VARS KB: " + e.getMessage());
                }
            }
            logger.debug("Done pushing to repository");
        }
    }
}
