package org.mbari.its;

import com.mongodb.AggregationOutput;
import com.mongodb.BasicDBObject;
import com.mongodb.Block;
import com.mongodb.DBObject;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bson.Document;

import java.util.Calendar;
import java.util.TimeZone;

import static java.util.Arrays.asList;

/**
 * Created by kgomes on 5/2/16.
 */
public class RepositoryManager {

    /**
     * The logger for this class
     */
    private static final Logger logger = LogManager.getLogger(RepositoryManager.class);

    /**
     * This is the MongoDB instance that this RepositoryManager will work with
     */
    private MongoDatabase mongoDatabase = null;

    /**
     * The constructor
     *
     * @param mongoDatabase
     */
    public RepositoryManager(MongoDatabase mongoDatabase) {
        // Set the MongoDB instance
        this.mongoDatabase = mongoDatabase;
    }

    /**
     * This method runs all the aggregations on the MongoDB collections
     */
    public void runMongoAggregations() {
        logger.debug("Going to run MongoDB aggregations");

        // First drop the exiting collections
        mongoDatabase.getCollection("agg_mw_species_year_month").drop();
        mongoDatabase.getCollection("agg_surface_year_month").drop();

        mongoDatabase.createCollection("agg_mw_species_year_month");
        mongoDatabase.createCollection("agg_surface_year_month");

        // Grab the midwater collection
        MongoCollection midwaterCollection = mongoDatabase.getCollection("midwater_counts");

        // Create the group object
        DBObject midwaterGroup = new BasicDBObject("$group", new BasicDBObject(
                "_id", new BasicDBObject("name", "$name")
                .append("month", new BasicDBObject("$month", "$date"))
                .append("year", new BasicDBObject("$year", "$date")))
                .append("value", new BasicDBObject("$sum", "$value")));

        // Now the sort object
        DBObject midwaterSort = new BasicDBObject("$sort", new BasicDBObject("_id.name", 1).append("_id.year", 1).append("_id.month", 1));

        // Perform the aggregation
        AggregateIterable<Document> midwaterOutput = midwaterCollection.aggregate(asList(midwaterGroup, midwaterSort));

        // Grab the correct collection
        MongoCollection aggMidwaterCollection = mongoDatabase.getCollection("agg_mw_species_year_month");

        logger.debug("Staring midwater aggregation");
        // Insert the records
        midwaterOutput.forEach(new Block<Document>() {
            @Override
            public void apply(final Document document) {
                // First pull out the year and month from the incoming document
                Integer yearInt = (Integer) ((Document) document.get("_id")).get("year");
                Integer monthInt = (Integer) ((Document) document.get("_id")).get("month");

                // Make sure we have values before going on
                if (yearInt != null && monthInt != null) {
                    // Now before we insert it, we need to add a date field, first create the date
                    Calendar date = Calendar.getInstance();
                    date.setTimeZone(TimeZone.getTimeZone("GMT"));
                    date.set(Calendar.YEAR, yearInt);
                    date.set(Calendar.MONTH, monthInt - 1);
                    date.set(Calendar.DAY_OF_MONTH, 1);
                    date.set(Calendar.HOUR_OF_DAY, 0);
                    date.set(Calendar.MINUTE, 0);
                    date.set(Calendar.SECOND, 0);
                    date.set(Calendar.MILLISECOND, 0);

                    // Now add the date to the document
                    document.append("date", date.getTime());

                    // Now insert it
                    aggMidwaterCollection.insertOne(document);
                } else {
                    logger.warn("Could not parse year or month from document");
                    logger.warn("Year -> " + ((Document) document.get("_id")).get("year"));
                    logger.warn("Month -> " + ((Document) document.get("_id")).get("month"));
                }
            }
        });
        logger.debug("Done with midwater aggregations");

        // Grab the surface collection
        MongoCollection surfaceCollection = mongoDatabase.getCollection("surface_data");

        // Create the group object
        DBObject surfaceGroup = new BasicDBObject("$group", new BasicDBObject(
                "_id", new BasicDBObject("name", "$name")
                .append("month", new BasicDBObject("$month", "$date"))
                .append("year", new BasicDBObject("$year", "$date")))
                .append("value", new BasicDBObject("$sum", "$value")));

        // Now the sort object
        DBObject surfaceSort = new BasicDBObject("$sort", new BasicDBObject("_id.name", 1).append("_id.year", 1).append("_id.month", 1));

        // Perform the aggregation
        AggregateIterable<Document> surfaceOutput = surfaceCollection.aggregate(asList(surfaceGroup, surfaceSort));

        // Grab the correct collection
        MongoCollection aggSurfaceCollection = mongoDatabase.getCollection("agg_surface_year_month");

        logger.debug("Starting surface aggregations");
        // Insert the records
        surfaceOutput.forEach(new Block<Document>() {
            @Override
            public void apply(final Document document) {
                // First pull out the year and month from the incoming document
                Integer yearInt = (Integer) ((Document) document.get("_id")).get("year");
                Integer monthInt = (Integer) ((Document) document.get("_id")).get("month");

                // Now before we insert it, we need to add a date field, first create the date
                Calendar date = Calendar.getInstance();
                date.setTimeZone(TimeZone.getTimeZone("GMT"));
                date.set(Calendar.YEAR, yearInt);
                date.set(Calendar.MONTH, monthInt - 1);
                date.set(Calendar.DAY_OF_MONTH, 1);
                date.set(Calendar.HOUR_OF_DAY, 0);
                date.set(Calendar.MINUTE, 0);
                date.set(Calendar.SECOND, 0);
                date.set(Calendar.MILLISECOND, 0);

                // Now add the date to the document
                document.append("date", date.getTime());

                aggSurfaceCollection.insertOne(document);
            }
        });
        logger.debug("Done with surface aggregations");

    }
}
