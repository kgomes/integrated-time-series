package org.mbari.its;

import com.mongodb.client.MongoCollection;
import jdk.nashorn.internal.runtime.regexp.RegExp;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bson.Document;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by kgomes on 5/2/16.
 */
public class DayNightExtractor {

    /**
     * The logger for this class
     */
    private static final Logger logger = LogManager.getLogger(DayNightExtractor.class);

    /**
     * This is the Mongo collection where the data will be written to
     */
    private MongoCollection mongoCollection = null;

    /**
     * This is the URL where the day night data resides
     */
    private String dayNightUrl = null;

    Pattern timePattern = Pattern.compile("(\\d+):(\\d+)\\s+(\\S+)\\s+\\S+");
    private SimpleDateFormat dayNightDateFormat = new SimpleDateFormat("h:m aa", Locale.ENGLISH);

    /**
     * The constructor
     *
     * @param mongoCollection
     * @param dayNightUrl
     */
    public DayNightExtractor(MongoCollection mongoCollection, String dayNightUrl) {
        // Set the mongo collection
        this.mongoCollection = mongoCollection;

        // Set the URL of the MEI data
        this.dayNightUrl = dayNightUrl;
    }

    /**
     * This method grabs the moon and sun data from the URL and pushes it into a Mongo collection
     */
    public void extractData() {
        logger.debug("Starting extraction of Day Night data");

        // Drop the collection (for now)
        mongoCollection.drop();

        // Grab now
        Calendar now = Calendar.getInstance();

        // To start, let's create a date that starts on 1/1/1980
        Calendar queryDate = Calendar.getInstance();
        queryDate.setTimeZone(TimeZone.getTimeZone("America/Los_Angeles"));
        queryDate.set(Calendar.YEAR, 1980);
        queryDate.set(Calendar.MONTH, 1);
        queryDate.set(Calendar.DAY_OF_MONTH, 1);
        while (queryDate.get(Calendar.YEAR) <= now.get(Calendar.YEAR)) {
            logger.debug("Getting day night for " + (queryDate.get(Calendar.MONTH) + 1) + "/" +
                    queryDate.get(Calendar.DAY_OF_MONTH) + "/" + queryDate.get(Calendar.YEAR));

            // Create start date parameter
            String dateParam = "&date=" + (queryDate.get(Calendar.MONTH) + 1) + "/" +
                    queryDate.get(Calendar.DAY_OF_MONTH) + "/" + queryDate.get(Calendar.YEAR);

            // Create the URL to the page
            URL website = null;
            try {
                logger.debug("URL call: " + dayNightUrl + dateParam);
                website = new URL(dayNightUrl + dateParam);
            } catch (MalformedURLException e) {
                logger.error("MalformedURLException caught trying to create URL from " + dayNightUrl + dateParam + ": " + e.getMessage());
            }

            // Check that we have a valid URL
            if (website != null) {
                // Now grab a connection
                URLConnection connection = null;
                try {
                    connection = website.openConnection();
                } catch (IOException e) {
                    logger.error("IOException trying to connect to " + dayNightUrl + ": " + e.getMessage());
                }

                // Check for the connection
                if (connection != null) {

                    // Create an InputStream
                    InputStream in = null;
                    try {
                        in = connection.getInputStream();
                    } catch (IOException e) {
                        logger.error("IOException caught trying to create InputStream from website: " + e.getMessage());
                    }

                    // Make sure we have the reader
                    if (in != null) {
                        // Create a byte array output stream to write entire results to
                        ByteArrayOutputStream baos = new ByteArrayOutputStream();

                        // And a buffer to hold streaming data
                        byte[] buf = new byte[8192];

                        // Now read in the entire response
                        int len = 0;
                        try {
                            while ((len = in.read(buf)) != -1) {
                                baos.write(buf, 0, len);
                            }
                        } catch (IOException e) {
                            logger.error("IOException trying to read tide data from URL");
                        }

                        String body = null;
                        try {
                            body = new String(baos.toByteArray(), "UTF-8");
                        } catch (UnsupportedEncodingException e) {
                            logger.error("UnsupportedEncodingException caught trying to convert page to string: " +
                                    e.getMessage());
                        }

                        // The list of documents that will be inserted
                        List<Document> documents = new ArrayList<Document>();

                        // Convert the JSON string to an object
                        JSONParser jsonParser = new JSONParser();

                        try {
                            JSONObject dayNightJSON = (JSONObject) jsonParser.parse(body);

                            // Look for the sundata array
                            JSONArray sunDataJSON = (JSONArray) dayNightJSON.get("sundata");

                            // Loop over the entries and look for sunrise, sunset
                            for (int i = 0; i < sunDataJSON.size(); i++) {
                                JSONObject sundataEntry = (JSONObject) sunDataJSON.get(i);
                                if (sundataEntry.get("phen").equals("R")) {
                                    Matcher timeMatcher = timePattern.matcher(sundataEntry.get("time").toString());
                                    if (timeMatcher.matches()) {
                                        int hour = Integer.parseInt(timeMatcher.group(1));
                                        int minute = Integer.parseInt(timeMatcher.group(2));
                                        queryDate.set(Calendar.HOUR_OF_DAY, hour);
                                        queryDate.set(Calendar.MINUTE, minute);
                                    }
                                    // Create a new entry for sunrise
                                    Document newDocument = new Document("date", queryDate.getTime())
                                            .append("date-resolution", "minute")
                                            .append("name", "sun-visible")
                                            .append("value", 1)
                                            .append("units", "boolean");
                                    documents.add(newDocument);
                                } else if (sundataEntry.get("phen").equals("S")) {
                                    Matcher timeMatcher = timePattern.matcher(sundataEntry.get("time").toString());
                                    if (timeMatcher.matches()) {
                                        int hour = Integer.parseInt(timeMatcher.group(1)) + 12;
                                        int minute = Integer.parseInt(timeMatcher.group(2));
                                        logger.debug("AM/PM: |" + timeMatcher.group(3) + "|");
                                        queryDate.set(Calendar.HOUR_OF_DAY, hour);
                                        queryDate.set(Calendar.MINUTE, minute);
                                    }
                                    // Create a new entry for sunset
                                    Document newDocument = new Document("date", queryDate.getTime())
                                            .append("date-resolution", "minute")
                                            .append("name", "sun-visible")
                                            .append("value", 0)
                                            .append("units", "boolean");
                                    documents.add(newDocument);
                                }
                            }
                        } catch (org.json.simple.parser.ParseException e) {
                            logger.error("ParseException trying to parse the JSON string " + e.getMessage());
                        }

                        // Now insert them
                        mongoCollection.insertMany(documents);
                    }
                }
            }

            // Add a day
            queryDate.add(Calendar.DATE, 1);
        }

        logger.debug("Done with Day Night extraction");
    }
}