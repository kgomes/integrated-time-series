package org.mbari.its;

import com.mongodb.client.MongoCollection;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bson.Document;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.Arrays.asList;

/**
 * Created by kgomes on 5/2/16.
 */
public class MEIExtractor {

    /**
     * The logger for this class
     */
    private static final Logger logger = LogManager.getLogger(MEIExtractor.class);

    /**
     * This is the Mongo collection where the data will be written to
     */
    private MongoCollection mongoCollection = null;

    /**
     * This is the URL where the MEI data resides
     */
    private String meiUrl = null;

    /**
     * The constructor
     *
     * @param mongoCollection
     * @param meiUrl
     */
    public MEIExtractor(MongoCollection mongoCollection, String meiUrl) {
        // Set the mongo collection
        this.mongoCollection = mongoCollection;

        // Set the URL of the MEI data
        this.meiUrl = meiUrl;
    }

    /**
     * This method grabs the MEI data from the MEI URL and pushes it into a Mongo collection
     */
    public void extractData() {
        logger.debug("Starting extraction of MEI data");

        // Create the URL to the page
        URL website = null;
        try {
            website = new URL(meiUrl);
        } catch (MalformedURLException e) {
            logger.error("MalformedURLException caught trying to create URL from " + meiUrl + ": " + e.getMessage());
        }

        // Check that we have a valid URL
        if (website != null) {
            // Now grab a connection
            URLConnection connection = null;
            try {
                connection = website.openConnection();
            } catch (IOException e) {
                logger.error("IOException trying to connect to " + meiUrl + ": " + e.getMessage());
            }

            // Check for the connection
            if (connection != null) {

                // Create a BufferedReader
                BufferedReader in = null;
                try {
                    in = new BufferedReader(
                            new InputStreamReader(
                                    connection.getInputStream()));
                } catch (IOException e) {
                    logger.error("IOException caught trying to create BufferedReader from website: " + e.getMessage());
                }

                // Make sure we have the reader
                if (in != null) {
                    // Drop the collection (for now)
                    mongoCollection.drop();

                    // The list of documents that will be inserted
                    List<Document> documents = new ArrayList<Document>();

                    // Create the regular expression to look for the data lines
                    Pattern pattern = Pattern.compile("^(\\d{4})\\s*(-*\\d*\\.\\d+)\\s*(-*\\d*\\.\\d+)*" +
                            "\\s*(-*\\d*\\.\\d+)*\\s*(-*\\d*\\.\\d+)*\\s*(-*\\d*\\.\\d+)*\\s*(-*\\d*\\.\\d+)*" +
                            "\\s*(-*\\d*\\.\\d+)*\\s*(-*\\d*\\.\\d+)*\\s*(-*\\d*\\.\\d+)*\\s*(-*\\d*\\.\\d+)*" +
                            "\\s*(-*\\d*\\.\\d+)*\\s*(-*\\d*\\.\\d+)*");

                    // Create the string to hold the next line from the site
                    String inputLine;

                    // Now start reading line by line
                    try {
                        while ((inputLine = in.readLine()) != null) {
                            Matcher matcher = pattern.matcher(inputLine);
                            if (matcher.matches()) {
                                logger.debug(inputLine);
                                // Create a new Calendar object
                                Calendar calendar = Calendar.getInstance();

                                // Clear the day of the month, hours, minutes and seconds
                                calendar.set(Calendar.DAY_OF_MONTH, 1);
                                calendar.set(Calendar.HOUR_OF_DAY, 0);
                                calendar.set(Calendar.MINUTE, 0);
                                calendar.set(Calendar.SECOND, 0);
                                calendar.set(Calendar.MILLISECOND, 0);

                                // Set the year from the first group
                                int year = Integer.parseInt(matcher.group(1).trim());
                                logger.debug("Year = " + year);
                                calendar.set(Calendar.YEAR, year);

                                // For each group, if they are not null, create a new document and add it
                                for (int i = 0; i < 12; i++) {
                                    // Set the month on the calendar
                                    calendar.set(Calendar.MONTH, i);
                                    logger.debug("After month set, date is " + calendar.getTime());

                                    // Check to see if the group was found
                                    String group = matcher.group(i + 2);
                                    if (group != null) {
                                        logger.debug("Adding value of " + group);
                                        Document newDocument = new Document("date", calendar.getTime())
                                                .append("date-resolution", "month")
                                                .append("name", "MEI")
                                                .append("value", Float.parseFloat(group.trim()))
                                                .append("units", "Standardized Departure");

                                        // Add it to the list
                                        documents.add(newDocument);
                                    }
                                }
                            }
                        }
                    } catch (IOException e) {
                        logger.error("IOException caught trying to read lines from the website: " + e.getMessage());
                    }

                    // Close up the input stream to tidy up
                    try {
                        in.close();
                    } catch (IOException e) {
                        logger.error("IOException caught trying to close reader from website: " + e.getMessage());
                    }

                    // Commit the documents to the database
                    // Write to the MongoDB
                    mongoCollection.insertMany(documents);
                }
            }
        }
        logger.debug("Done with MEI extraction");
    }
}