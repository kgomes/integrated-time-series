package org.mbari.its;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoDatabase;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bson.Document;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Level;

import static java.util.Arrays.asList;

/**
 * Hello world!
 */
public class DataCombiner {
    /**
     * The logger for this class
     */
    private static final Logger logger = LogManager.getLogger(DataCombiner.class);

    /**
     * These are the configuration properties for the DataCombiner
     */
    private Properties properties = new Properties();

    /**
     * This property indicates if the combiner should overwrite past results
     */
    private boolean overwriteResults = true;

    /**
     * The MongoDB where the data will be stored
     */
    private MongoDatabase mongoDatabase = null;

    /**
     * The MongoDB client object used to interact with the MongoDB instance
     */
    private MongoClient mongoClient = null;

    /**
     * The is the extractor for the surface data
     */
    private SurfaceExtractor surfaceExtractor = null;

    /**
     * This is the extractor responsible for the midwater data
     */
    private MidwaterExtractor midwaterExtractor = null;

    /**
     * This is the extractor for the benthic data
     */
    private BenthicExtractor benthicExtractor = null;

    /**
     * This is the extractor for the MEI data
     */
    private MEIExtractor meiExtractor = null;

    /**
     * This is the extractor for tide data
     */
    private TideExtractor tideExtractor = null;

    /**
     * This is the extractor for day night data
     */
    private DayNightExtractor dayNightExtractor = null;

    /**
     * The extractor the Ekman transport data
     */
    private EkmanExtractor ekmanExtractor = null;

    /**
     * This is the class that handles some of the MongoDB interactions
     */
    private RepositoryManager repositoryManager = null;

    /**
     * This is a date and time of the start of the combiner run
     */
    private Calendar runDate = Calendar.getInstance();

    /**
     * The format of the date string for the directories to hold the run data
     */
    private SimpleDateFormat directoryDateFormat = new SimpleDateFormat("YYYY-MM-dd-HH-mm");

    /**
     * This is the directory that will hold the data generated from this run
     */
    private File dataDirectory = null;

    /**
     * The default constructor
     */
    public DataCombiner() {
    }

    /**
     * This method initializes the DataCombiner by making sure all the database connections
     * are there and resources are all found
     *
     * @return
     */
    public boolean init() {
        // First thing to do is read in the config properties for the application
        InputStream inputStream = null;
        try {
            inputStream = DataCombiner.class.getClassLoader().getResourceAsStream("config.properties");
            properties.load(inputStream);
            System.out.println("Properties = " + properties);
        } catch (FileNotFoundException e) {
            logger.error("Config.properties not found!");
            e.printStackTrace();
            return false;
        } catch (IOException e) {
            logger.error("IOException trying to read config.properties!");
            e.printStackTrace();
            return false;
        }

        // Create a default data directory
        this.dataDirectory = new File("./data");

        // Now check to see if a different data directory was specified
        if (properties.getProperty("data.directory") != null) {
            // Create the epoch subdirectory
            this.dataDirectory = new File(properties.getProperty("data.directory"));
        }

        // Now make the data directory if it does not exist
        if (!this.dataDirectory.exists()) {
            this.dataDirectory.mkdir();
        }

        // Make sure it's a directory
        if (!dataDirectory.isDirectory()) {
            logger.error("A file named data was found, but it was not a directory. " +
                    " This application needs a directory named data");
            return false;
        }

        // Check to see if the overwrite.results property was set
        if (properties.getProperty("overwrite.results") != null &&
                !Boolean.parseBoolean(properties.getProperty("overwrite.results"))) {
            // Create the epoch subdirectory
            this.dataDirectory = new File(this.dataDirectory.getAbsolutePath() + File.separator +
                    directoryDateFormat.format(this.runDate.getTime()));
            this.dataDirectory.mkdir();
        }
        logger.info("Data directory is located at: " + this.dataDirectory.getAbsolutePath());

        // Create the connection to the MongoDB
        try {
            MongoClientURI connectionString = new MongoClientURI(properties.getProperty("mongodb.uri"));
            mongoClient = new MongoClient(connectionString);
            mongoDatabase = mongoClient.getDatabase(properties.getProperty("mongodb.name"));
        } catch (Exception e) {
            logger.error("Connection to MongoDB failed: " + e.getMessage());
            return false;
        }

        // Create the repository manager
        this.repositoryManager = new RepositoryManager(mongoDatabase);

        // Verify the connection that the surface extractor is using
        if (properties.getProperty("bog.db.connection.url") == null ||
                properties.getProperty("bog.db.connection.url") == "") {
            logger.error("No bog.db.connection.url property was found in config.properties");
            return false;
        } else {
            // We should have the connection URL, so let's try to create the BOG object
            surfaceExtractor =
                    new SurfaceExtractor(properties.getProperty("bog.db.connection.url"),
                            properties.getProperty("bog.db.table.taxa.columns"),
                            properties.getProperty("bog.db.table.taxa-al.columns"),
                            this.mongoDatabase.getCollection("surface_data"));

            // And test the connection to the DB
            if (!surfaceExtractor.testConnection()) {
                return false;
            }
        }

        // Verify the connection that the midwater extractor is using
        if (properties.getProperty("midwater.db.connection.url") == null ||
                properties.getProperty("midwater.db.connection.url") == "") {
            logger.error("No midwater.db.connection.url property was found in config.properties");
            return false;
        } else {
            if (properties.getProperty("vars.db.connection.url") == null ||
                    properties.getProperty("vars.db.connection.url") == "") {
                logger.error("No vars.db.connection.url property was found in config.properties");
                return false;
            } else {
                // The list of concepts to query for from the properties file
                String[] conceptList = null;

                // Check to see if there was a list of species in the config file
                if (properties.getProperty("vars.db.concept.list") != null) {
                    // Split the property by comma into an array
                    String[] conceptListFromProperty = properties.getProperty("vars.db.concept.list").split(",");
                    if (conceptListFromProperty.length > 0) {
                        logger.info("This run will query VARS for " + conceptListFromProperty.length +
                                " concepts defined in the configuration file");
                        conceptList = conceptListFromProperty;
                    } else {
                        logger.info("This run will query VARS for all concepts");
                    }
                }

                // We should have everything we need, let's build the midwater extractor
                midwaterExtractor =
                        new MidwaterExtractor(properties.getProperty("midwater.db.connection.url"),
                                properties.getProperty("vars.db.connection.url"),
                                properties.getProperty("varskb.db.connection.url"),
                                properties.getProperty("phylogeny.url.base"), conceptList,
                                this.dataDirectory, mongoDatabase.getCollection("midwater_counts"));

                // And test the connection to the DB
                if (!midwaterExtractor.testConnection()) {
                    return false;
                }
            }
        }

        // Grab the benthic data spreadsheet location from properties
        if (properties.getProperty("benthic.data.spreadsheet") == null ||
                properties.getProperty("benthic.data.spreadsheet") == "") {
            logger.error("No benthic spreadsheet was defined in config.properties");
            return false;
        } else {
            // Create the new BenthicExtractor
            benthicExtractor = new BenthicExtractor(properties.getProperty("benthic.data.spreadsheet"),
                    this.mongoDatabase.getCollection("benthic_counts"));
        }

        // Create new MEI extractor
        if (properties.getProperty("mei.data.url") == null ||
                properties.getProperty("mei.data.url") == "") {
            logger.error("No MEI URL was defined in config.properties");
            return false;
        } else {
            meiExtractor = new MEIExtractor(mongoDatabase.getCollection("mei_data"),
                    properties.getProperty("mei.data.url"));
        }

        // Create a new Tide extractor
        if (properties.getProperty("tide.data.url") == null ||
                properties.getProperty("tide.data.url") == "") {
            logger.error("No Tide URL was defined in config.properties");
            return false;
        } else {
            tideExtractor = new TideExtractor(mongoDatabase.getCollection("tide_data"),
                    properties.getProperty("tide.data.url"));
        }

        // Create a new Day Night extractor
        if (properties.getProperty("daynight.data.url") == null ||
                properties.getProperty("daynight.data.url") == "") {
            logger.error("No Day Night URL was defined in config.properties");
            return false;
        } else {
            dayNightExtractor = new DayNightExtractor(mongoDatabase.getCollection("daynight_data"),
                    properties.getProperty("daynight.data.url"));
        }

        if (properties.getProperty("ekman.data.url") == null ||
                properties.getProperty("ekman.data.url") == "") {
            logger.error("No Ekman URL was defined in config.properties");
            return false;
        } else {
            ekmanExtractor = new EkmanExtractor(mongoDatabase.getCollection("ekman_data"),
                    properties.getProperty("ekman.data.url"));
        }

        // Since we got here, return that it looks like the init went OK
        return true;
    }

    /**
     * This method fires off the activity to query for all the data and combine it in a central data store
     */

    public void combineData() {
        // Run the surface extractor
        //logger.debug("Starting surface data extraction");
        //surfaceExtractor.extractData();

        // Run the midwater extraction
        //logger.debug("Starting midwater extraction");
        //midwaterExtractor.extractData();

        // Read in the benthic data from the spreadsheet
        //logger.debug("Starting benthic extraction");
        //benthicExtractor.extractData();

        // Run the MEI extraction
        //logger.debug("Starting mei extraction");
        //meiExtractor.extractData();

        // Run the tide extraction
        //logger.debug("Starting tide extraction");
        //tideExtractor.extractData();

        // Run the day night extraction
        //logger.debug("Starting day night extraction");
        //dayNightExtractor.extractData();

        // Run the ekman extraction
        logger.debug("Starting ekman extraction");
        ekmanExtractor.extractData();

        // Now run the aggregations
        //logger.debug("Staring aggregations");
        //repositoryManager.runMongoAggregations();

        // Close up the DB connection
        logger.debug("All done, closing up shop");
        mongoClient.close();
    }

    /**
     * The main method to start off the DataCombiner
     *
     * @param args
     */

    public static void main(String[] args) {
        logger.info("========================= DataCombiner started at " + new Date() + " ==========================");

        java.util.logging.Logger mongoLogger = java.util.logging.Logger.getLogger("org.mongodb");
        mongoLogger.setLevel(Level.SEVERE);
        // Create a new DataCombiner
        DataCombiner dataCombiner = new DataCombiner();

        // Initialize it
        boolean initResult = dataCombiner.init();

        // Check to see if initialization went OK
        if (initResult) {
            dataCombiner.combineData();
        } else {
            System.out.println("Could not initialize the DataCombiner, " +
                    "please check the logs directory for some indication as to why");
        }
    }
}
