package org.mbari.its;

import com.mongodb.client.MongoCollection;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bson.Document;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by kgomes on 5/2/16.
 */
public class EkmanExtractor {

    /**
     * The logger for this class
     */
    private static final Logger logger = LogManager.getLogger(EkmanExtractor.class);

    /**
     * This is the Mongo collection where the data will be written to
     */
    private MongoCollection mongoCollection = null;

    /**
     * This is the URL where the Ekman data resides
     */
    private String ekmanUrl = null;

    private String missingValueString = "-1e+34";

    //private SimpleDateFormat tideDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH);

    // The alpha value used in the calculations
    private Double alpha = .6435;

    // U component pattern
    // WIND_U_COMPONENT_DAY.WIND_U_COMPONENT_DAY[WIND_U_COMPONENT_DAY.TIME_DAY=32360][WIND_U_COMPONENT_DAY.DEPTH_MET_HR=-3.5][WIND_U_COMPONENT_DAY.LATITUDE_MET_HR=36.75], -1e+34
    private Pattern uComponentPattern = Pattern.compile("WIND_U_COMPONENT_DAY\\.WIND_U_COMPONENT_DAY\\[WIND_U_COMPONENT_DAY\\.TIME_DAY=(\\d+)\\]\\[WIND_U_COMPONENT_DAY\\.DEPTH_MET_HR=\\S+\\]\\[WIND_U_COMPONENT_DAY\\.LATITUDE_MET_HR=\\S+\\],\\s+(-*\\S+)");

    // V component pattern
    // WIND_V_COMPONENT_DAY.WIND_V_COMPONENT_DAY[WIND_V_COMPONENT_DAY.TIME_DAY=32360][WIND_V_COMPONENT_DAY.DEPTH_MET_HR=-3.5][WIND_V_COMPONENT_DAY.LATITUDE_MET_HR=36.75], -1e+34
    private Pattern vComponentPattern = Pattern.compile("WIND_V_COMPONENT_DAY\\.WIND_V_COMPONENT_DAY\\[WIND_V_COMPONENT_DAY\\.TIME_DAY=(\\d+)\\]\\[WIND_V_COMPONENT_DAY\\.DEPTH_MET_HR=\\S+\\]\\[WIND_V_COMPONENT_DAY\\.LATITUDE_MET_HR=\\S+\\],\\s+(-*\\S+)");

    /**
     * The constructor
     *
     * @param mongoCollection
     * @param ekmanUrl
     */
    public EkmanExtractor(MongoCollection mongoCollection, String ekmanUrl) {
        // Set the mongo collection
        this.mongoCollection = mongoCollection;

        // Set the URL of the ekman data
        this.ekmanUrl = ekmanUrl;
    }

    /**
     * This method grabs the ekman data from the tide URL and pushes it into a Mongo collection
     */
    public void extractData() {
        logger.debug("Starting extraction of Ekman data");

        // Drop the collection (for now)
        mongoCollection.drop();

        // Create a date object to start at 1901
        Calendar baseDate = Calendar.getInstance();
        baseDate.set(Calendar.YEAR, 1901);
        baseDate.set(Calendar.MONTH, 1);
        baseDate.set(Calendar.DAY_OF_MONTH, 15);
        baseDate.set(Calendar.HOUR_OF_DAY, 0);
        baseDate.set(Calendar.MINUTE, 0);
        baseDate.set(Calendar.SECOND, 0);

        // This extraction actually has to be done in two loops.  I need to grab the U and V components of wind
        // and make a calculation with them.  This is a map that will contain entries that have day since 1901 as
        // they keys and arrays of [u,v] as values
        TreeMap<Integer, Double[]> dataMap = new TreeMap<>();

        // Create the URL to the page for the U component first
        URL website = null;
        try {
            logger.debug("URL call: " + ekmanUrl + "WIND_U_COMPONENT_DAY[0:1:9871][0:1:0][0:1:0][0:1:0]");
            website = new URL(ekmanUrl + "WIND_U_COMPONENT_DAY[0:1:9871][0:1:0][0:1:0][0:1:0]");
        } catch (MalformedURLException e) {
            logger.error("MalformedURLException caught trying to create URL from " + ekmanUrl + "WIND_U_COMPONENT_DAY[0:1:9871][0:1:0][0:1:0][0:1:0]" + ": " + e.getMessage());
        }

        // Check that we have a valid URL
        if (website != null) {
            // Now grab a connection
            URLConnection connection = null;
            try {
                connection = website.openConnection();
            } catch (IOException e) {
                logger.error("IOException trying to connect to " + ekmanUrl + ": " + e.getMessage());
            }

            // Check for the connection
            if (connection != null) {

                // Create a BufferedReader
                BufferedReader in = null;
                try {
                    in = new BufferedReader(
                            new InputStreamReader(
                                    connection.getInputStream()));
                } catch (IOException e) {
                    logger.error("IOException caught trying to create BufferedReader from website: " + e.getMessage());
                }

                // Make sure we have the reader
                if (in != null) {
                    // Create the string to hold the next line from the site
                    String inputLine;

                    // Now start reading line by line
                    try {
                        while ((inputLine = in.readLine()) != null) {
                            Matcher matcher = uComponentPattern.matcher(inputLine);
                            if (matcher.matches()) {
                                if (!matcher.group(2).equals(missingValueString)) {
                                    dataMap.put(Integer.parseInt(matcher.group(1)), new Double[]{Double.parseDouble(matcher.group(2)), null});
                                }
                            }
                        }
                    } catch (IOException e) {
                        logger.error("IOException caught trying to read lines from the website: " + e.getMessage());
                    }

                    // Close up the input stream to tidy up
                    try {
                        in.close();
                    } catch (IOException e) {
                        logger.error("IOException caught trying to close reader from website: " + e.getMessage());
                    }

                }
            }
        }
        // Create the URL to the page for the V component
        try {
            logger.debug("URL call: " + ekmanUrl + "WIND_V_COMPONENT_DAY[0:1:9871][0:1:0][0:1:0][0:1:0]");
            website = new URL(ekmanUrl + "WIND_V_COMPONENT_DAY[0:1:9871][0:1:0][0:1:0][0:1:0]");
        } catch (MalformedURLException e) {
            logger.error("MalformedURLException caught trying to create URL from " + ekmanUrl + "WIND_V_COMPONENT_DAY[0:1:9871][0:1:0][0:1:0][0:1:0]" + ": " + e.getMessage());
        }

        // Check that we have a valid URL
        if (website != null) {
            // Now grab a connection
            URLConnection connection = null;
            try {
                connection = website.openConnection();
            } catch (IOException e) {
                logger.error("IOException trying to connect to " + ekmanUrl + ": " + e.getMessage());
            }

            // Check for the connection
            if (connection != null) {

                // Create a BufferedReader
                BufferedReader in = null;
                try {
                    in = new BufferedReader(
                            new InputStreamReader(
                                    connection.getInputStream()));
                } catch (IOException e) {
                    logger.error("IOException caught trying to create BufferedReader from website: " + e.getMessage());
                }

                // Make sure we have the reader
                if (in != null) {
                    // Create the string to hold the next line from the site
                    String inputLine;

                    // Now start reading line by line
                    try {
                        while ((inputLine = in.readLine()) != null) {
                            Matcher matcher = vComponentPattern.matcher(inputLine);
                            if (matcher.matches()) {
                                dataMap.put(Integer.parseInt(matcher.group(1)), new Double[]{Double.parseDouble(matcher.group(2)), null});
                                if (dataMap.get(Integer.parseInt(matcher.group(1))) != null && !matcher.group(2).equals(missingValueString)) {
                                    dataMap.get(Integer.parseInt(matcher.group(1)))[1] = Double.parseDouble(matcher.group(2));
                                }
                            }
                        }
                    } catch (IOException e) {
                        logger.error("IOException caught trying to read lines from the website: " + e.getMessage());
                    }

                    // Close up the input stream to tidy up
                    try {
                        in.close();
                    } catch (IOException e) {
                        logger.error("IOException caught trying to close reader from website: " + e.getMessage());
                    }

                }
            }
        }
        // Drop the collection (for now)
        mongoCollection.drop();

        // The list of documents that will be inserted
        List<Document> documents = new ArrayList<Document>();

        // Now iterate over the days (keys)
        for (Map.Entry<Integer, Double[]> entry : dataMap.entrySet()) {
            Integer day = entry.getKey();
            Double[] uvData = entry.getValue();
            Calendar dataDate = Calendar.getInstance();
            dataDate.setTime(baseDate.getTime());
            dataDate.add(Calendar.DATE, day);
            if (uvData[0] != null && uvData[1] != null) {
                Double ekmanData = Math.sin(alpha) * uvData[0] + Math.cos(alpha) * uvData[1];
                logger.debug("Date: " + dataDate + ", Ekman: " + ekmanData);
                Document newDocument = new Document("date", dataDate.getTime())
                        .append("date-resolution", "day")
                        .append("name", "Ekman")
                        .append("value", ekmanData)
                        .append("units", "ekman");
                // Add it to the list
                documents.add(newDocument);
            } else {
                logger.debug("Null value at date: " + dataDate);
            }
        }

        // Write to the MongoDB
        mongoCollection.insertMany(documents);

        logger.debug("Done with ekman extraction");
    }
}