package org.mbari.its;

import com.mongodb.client.MongoCollection;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bson.Document;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;

/**
 * This class serves as the interface to the BOG database that we want to extract information from
 */
public class SurfaceExtractor {

    /**
     * The logger for this class
     */
    private static final Logger logger = LogManager.getLogger(SurfaceExtractor.class);

    /**
     * This is the connection to the BOG database
     */
    private String connectionUrl = null;

    /**
     * This is the array of column titles that are to be extracted from the Taxa table in the BOG DB
     */
    private String[] taxaColumns = null;

    /**
     * This is the array of column titles that are to be extracted from the Taxa_al table in the BOG DB
     */
    private String[] taxaAlColumns = null;

    /**
     * This is the directory where the extractor will write files
     */
//    private File dataDirectory = null;

    /**
     * This is the file that the surface data will be written to (CSV)
     */
//    private File surfaceDataFile = null;

    /**
     * The MongoDB collection to write data to
     */
    private MongoCollection mongoCollection = null;

    /**
     * The constructor
     *
     * @param connectionUrl
     * @param taxaTableColumns
     * @param taxaAlTableColumns
     * @param mongoCollection
     */
    public SurfaceExtractor(String connectionUrl, String taxaTableColumns, String taxaAlTableColumns,
                            MongoCollection mongoCollection) {
        // Set the connection Url
        this.connectionUrl = connectionUrl;

        // Check to see if we have the list of columns for the taxa table
        if (taxaTableColumns != null) {
            this.taxaColumns = taxaTableColumns.split(",");
        }

        // Check to see if we have the list of columns for the taxa_al table
        if (taxaAlTableColumns != null) {
            this.taxaAlColumns = taxaAlTableColumns.split(",");
        }

        // Set the data directory
//        this.dataDirectory = dataDirectory;

        // Set the mongo database
        this.mongoCollection = mongoCollection;

        // Create the necessary files
//        if (this.dataDirectory != null &&
//                this.dataDirectory.exists() && this.dataDirectory.isDirectory()) {
//
//            // Create the results file for BOG data
//            this.surfaceDataFile = new File(this.dataDirectory + File.separator + "bogExtraction.csv");
//        }
    }

    /**
     * This method attempt to establish a connection to the BOG database and reports back if it was
     * successful (true) or not (false)
     *
     * @return
     */
    public boolean testConnection() {
        // Try to testConnection with the URL given during construction
        Connection connection = null;

        try {
            connection = DriverManager.getConnection(this.connectionUrl);
        } catch (SQLException e) {
            logger.error("SQLException caught on connection: " + e.getMessage());
            return false;
        } finally {
            if (connection != null) try {
                connection.close();
            } catch (SQLException e) {
                logger.warn("SQLException caught trying to close the connection in the testConnection method: " +
                        e.getMessage());
            }
        }

        // Return that all was OK
        return true;
    }

    /**
     * This method queries the surface data for the data relevant to the Integrated Time Series and then writes
     * the results to the provided file and loads the data in the MongoDB
     *
     * @return the number of records that were extracted and written to a file
     */
    public void extractData() {

        // The number of rows found in the database
        int numRows = 0;

        // Grab a connection
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(this.connectionUrl);
        } catch (SQLException e) {
            logger.error("SQLException caught trying to get connection: " + e.getMessage());
        }

        // Run the query on the database if a connection was made
        if (connection != null) {
            // Create a statement
            Statement statement = null;
            try {
                statement = connection.createStatement();
            } catch (SQLException e) {
                logger.error("SQLException caught trying to create a statement: " + e.getMessage());
            }

            // If the statement was created OK, run the query
            ResultSet rs = null;
            if (statement != null) {
                // Create the SQL string based on the columns passed in at construction time
                StringBuilder queryStringBuilder = new StringBuilder();
                queryStringBuilder.append("select cruise, date_time, seq, ctrb_id, dec_lat, dec_long, depth");
                for (int i = 0; i < taxaColumns.length; i++) {
                    queryStringBuilder.append("," + taxaColumns[i]);
                }
                queryStringBuilder.append(" from taxa_v5 where (ctrb_id in ('M1','Mooring1','H3','67-50') and depth=0) order by date_time");
                try {
                    rs = statement.executeQuery(queryStringBuilder.toString());
                } catch (SQLException e) {
                    logger.error("SQLException caught trying to run the query: " + e.getMessage());
                }
            }

            // Loop over the rows and build the result
            if (rs != null) {
                // Drop the collection (for now)
                mongoCollection.drop();

                // The list of documents that will be inserted
                List<Document> documents = new ArrayList<Document>();

                // Build the CSV header using the configuration properties
//                StringBuilder headerBuilder = new StringBuilder();
//                headerBuilder.append("date_time, depth, dec_lat, dec_long, cruise, seq, ctrb_id,");

                // If a results file was provided, create a writer and write the results
//                FileWriter bogDataFileWriter = null;
                try {
//                    bogDataFileWriter = new FileWriter(surfaceDataFile);
//                    bogDataFileWriter.write("date_time, depth, dec_lat, dec_long, cruise, seq, ctrb_id, allp, allhet, " +
//                            "syn, rfp, prym, aflag, adino, cryp, prasy, phaeo, pen, cen, hdino, hflag, " +
//                            "choano, hcryp, hcil, acil\n");
//                    // TODO kgomes - the above header should come from the properties file as well so it matches data

                    while (rs.next()) {
                        numRows++;
                        // Since we want to be able to query for each 'column' as well as reduce the data using
                        // Mongo, I want to break out each data point into it's own Document
                        for (int i = 0; i < taxaColumns.length; i++) {

                            // Check to see if depth is greater than zero
                            float depthToRecord = rs.getFloat("depth");
                            if (rs.getFloat("depth") > 0) {
                                depthToRecord = -1 * depthToRecord;
                            }

                            // Create the BSON Document
                            Document newDocument = new Document("date", new java.util.Date(rs.getTimestamp("date_time").getTime()))
                                    .append("date-resolution", "day")
                                    .append("source", "bog-taxa-table")
                                    .append("loc",
                                            new Document("type", "Point")
                                                    .append("coordinates",
                                                            asList(rs.getFloat("dec_long"), rs.getFloat("dec_lat"),
                                                                    depthToRecord)))
                                    .append("depth", rs.getFloat("depth"))
                                    .append("name", taxaColumns[i])
                                    .append("value", rs.getFloat(taxaColumns[i]))
                                    .append("units", "ugC/l")
                                    .append("custom-properties",
                                            new Document("cruise", rs.getString("cruise"))
                                                    .append("seq", rs.getInt("seq"))
                                                    .append("ctrb_id", rs.getString("ctrb_id")));

                            // Add it to the list
                            documents.add(newDocument);
                        }
//                        // Write results to the file
//                        bogDataFileWriter.write(rs.getTimestamp("date_time") + "," +
//                                rs.getFloat("depth") + "," +
//                                rs.getFloat("dec_lat") + "," +
//                                rs.getFloat("dec_long") + "," +
//                                rs.getString("cruise") + "," +
//                                rs.getInt("seq") + "," +
//                                rs.getString("ctrb_id") + "," +
//                                rs.getFloat("allp") + "," +
//                                rs.getFloat("allhet") + "," +
//                                rs.getFloat("syn") + "," +
//                                rs.getFloat("rfp") + "," +
//                                rs.getFloat("prym") + "," +
//                                rs.getFloat("aflag") + "," +
//                                rs.getFloat("adino") + "," +
//                                rs.getFloat("cryp") + "," +
//                                rs.getFloat("prasy") + "," +
//                                rs.getFloat("phaeo") + "," +
//                                rs.getFloat("pen") + "," +
//                                rs.getFloat("cen") + "," +
//                                rs.getFloat("hdino") + "," +
//                                rs.getFloat("hflag") + "," +
//                                rs.getFloat("choano") + "," +
//                                rs.getFloat("hcryp") + "," +
//                                rs.getFloat("hcil") + "," +
//                                rs.getFloat("acil") + "\n");
//                        // TODO kgomes - the above should also be driven by the config.properties file
                    }

                    // Write to the MongoDB
                    mongoCollection.insertMany(documents);

                    // Close the file
//                    bogDataFileWriter.close();
                    logger.info(numRows + " BOG TAXA results found");
                } catch (SQLException e) {
                    logger.error("SQLException caught looping over the results of the query: " + e.getMessage());
                }
//                catch (IOException e) {
//                    logger.error("IOException caught trying to write rov start and end dates to file: " +
//                            e.getMessage());
//                }
            }

            // Try to close the result set
            if (rs != null) try {
                rs.close();
            } catch (SQLException e) {
                logger.warn("SQLException caught trying to close the resultset after query failed: " +
                        e.getMessage());
            }
            // Try to close the statement
            if (statement != null) try {
                statement.close();
            } catch (SQLException e) {
                logger.warn("SQLException caught trying to close the statement after query failed: " +
                        e.getMessage());
            }

            // Now let's query the taxa_al table
            if (connection != null) {
                // Create a statement
                statement = null;
                try {
                    statement = connection.createStatement();
                } catch (SQLException e) {
                    logger.error("SQLException caught trying to create a statement: " + e.getMessage());
                }

                // If the statement was created OK, run the query
                rs = null;
                if (statement != null) {
                    // Create the SQL string based on the columns passed in at construction time
                    StringBuilder queryStringBuilder = new StringBuilder();
                    queryStringBuilder.append("select max(cruise) as cruise, max(expd) as expd, max(seq) as seq, " +
                            "max(bottle) as bottle, max(ctrb_id) as ctrb_id, max(platform) as platform, date_time, " +
                            "max(dec_lat) as dec_lat, max(dec_long) as dec_long");
                    for (int i = 0; i < taxaAlColumns.length; i++) {
                        queryStringBuilder.append(",max(" + taxaAlColumns[i] + ") as " + taxaAlColumns[i]);
                    }
                    queryStringBuilder.append(" from taxa_al_v5 where (ctrb_id in ('M1','Mooring1','H3','67-50')) and depth = 0 group by standard_name, date_time order by date_time");
                    try {
                        rs = statement.executeQuery(queryStringBuilder.toString());
                    } catch (SQLException e) {
                        logger.error("SQLException caught trying to run the query: " + e.getMessage());
                    }
                }

                // Loop over the rows and build the result
                if (rs != null) {

                    // The list of documents that will be inserted
                    List<Document> documents = new ArrayList<Document>();

                    // Build the CSV header using the configuration properties
//                    StringBuilder headerBuilder = new StringBuilder();
//                    headerBuilder.append("date_time, depth, dec_lat, dec_long, cruise, seq, ctrb_id,");

                    // If a results file was provided, create a writer and write the results
//                    FileWriter bogDataFileWriter = null;
                    try {
//                        bogDataFileWriter = new FileWriter(surfaceDataFile);
//                        bogDataFileWriter.write("date_time, depth, dec_lat, dec_long, cruise, seq, ctrb_id, allp, allhet, " +
//                                "syn, rfp, prym, aflag, adino, cryp, prasy, phaeo, pen, cen, hdino, hflag, " +
//                                "choano, hcryp, hcil, acil\n");
//                        // TODO kgomes - the above header should come from the properties file as well so it matches data

                        while (rs.next()) {
                            numRows++;
                            // Since we want to be able to query for each 'column' as well as reduce the data using
                            // Mongo, I want to break out each data point into it's own Document
//                            for (int i = 0; i < taxaAlColumns.length; i++) {

                                // Check to see if depth is greater than zero
//                                float depthToRecord = rs.getFloat("depth");
//                                if (rs.getFloat("depth") > 0) {
//                                    depthToRecord = -1 * depthToRecord;
//                                }

                            // Check to see if we have a standard name
                            String name = rs.getString("standard_name");
                            if (name == null) {
                                name = "Unknown (Group " + rs.getInt("groups") + ")";
                            }
                                // Create the BSON Document
                                Document newDocument = new Document("date", new java.util.Date(rs.getTimestamp("date_time").getTime()))
                                        .append("date-resolution", "day")
                                        .append("source", "bog-taxa-al-table")
                                        .append("loc",
                                                new Document("type", "Point")
                                                        .append("coordinates",
                                                                asList(rs.getFloat("dec_long"), rs.getFloat("dec_lat"))))
                                        .append("name", name)
                                        .append("value", rs.getFloat("Carblit"))
                                        .append("units", "ugC/l")
                                        .append("custom-properties",
                                                new Document("cruise", rs.getString("cruise"))
                                                        .append("expd", rs.getInt("expd"))
                                                        .append("seq", rs.getInt("seq"))
                                                        .append("bottle", rs.getInt("bottle"))
                                                        .append("platform", rs.getString("platform"))
                                                        .append("ctrb_id", rs.getString("ctrb_id"))
                                                        .append("vf", rs.getFloat("vf"))
                                                        .append("groups", rs.getInt("groups"))
                                                        .append("shape", rs.getInt("shape"))
                                                        .append("length", rs.getInt("length"))
                                                        .append("width", rs.getInt("width"))
                                                        .append("thick", rs.getInt("thick"))
                                                        .append("perliter", rs.getInt("perliter"))
                                                        .append("volcell", rs.getFloat("volcell"))
                                                        .append("voliter", rs.getFloat("voliter"))
                                                        .append("carbcell", rs.getFloat("carbcell")));

                                // Add it to the list
                                documents.add(newDocument);
//                            }
                            // Write results to the file
//                            bogDataFileWriter.write(rs.getTimestamp("date_time") + "," +
//                                    rs.getFloat("depth") + "," +
//                                    rs.getFloat("dec_lat") + "," +
//                                    rs.getFloat("dec_long") + "," +
//                                    rs.getString("cruise") + "," +
//                                    rs.getInt("seq") + "," +
//                                    rs.getString("ctrb_id") + "," +
//                                    rs.getFloat("allp") + "," +
//                                    rs.getFloat("allhet") + "," +
//                                    rs.getFloat("syn") + "," +
//                                    rs.getFloat("rfp") + "," +
//                                    rs.getFloat("prym") + "," +
//                                    rs.getFloat("aflag") + "," +
//                                    rs.getFloat("adino") + "," +
//                                    rs.getFloat("cryp") + "," +
//                                    rs.getFloat("prasy") + "," +
//                                    rs.getFloat("phaeo") + "," +
//                                    rs.getFloat("pen") + "," +
//                                    rs.getFloat("cen") + "," +
//                                    rs.getFloat("hdino") + "," +
//                                    rs.getFloat("hflag") + "," +
//                                    rs.getFloat("choano") + "," +
//                                    rs.getFloat("hcryp") + "," +
//                                    rs.getFloat("hcil") + "," +
//                                    rs.getFloat("acil") + "\n");
//                            // TODO kgomes - the above should also be driven by the config.properties file
                        }

                        // Write to the MongoDB
                        mongoCollection.insertMany(documents);

                        // Close the file
//                        bogDataFileWriter.close();
                        logger.info(numRows + " BOG TAXA_AL results found");
                    } catch (SQLException e) {
                        logger.error("SQLException caught looping over the results of the query: " + e.getMessage());
                    }
//                    catch (IOException e) {
//                        logger.error("IOException caught trying to write rov start and end dates to file: " +
//                                e.getMessage());
//                    }
                }

                // Try to close the result set
                if (rs != null) try {
                    rs.close();
                } catch (SQLException e) {
                    logger.warn("SQLException caught trying to close the resultset after query failed: " +
                            e.getMessage());
                }
                // Try to close the statement
                if (statement != null) try {
                    statement.close();
                } catch (SQLException e) {
                    logger.warn("SQLException caught trying to close the statement after query failed: " +
                            e.getMessage());
                }

                // Try to close the connection
                if (connection != null) try {
                    connection.close();
                } catch (SQLException e) {
                    logger.warn("SQLException caught trying to close the connection after query failed: " +
                            e.getMessage());
                }
            }
        }

    }
}