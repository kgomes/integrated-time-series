package org.mbari.its;

import com.mongodb.client.MongoCollection;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bson.Document;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by kgomes on 5/2/16.
 */
public class TideExtractor {

    /**
     * The logger for this class
     */
    private static final Logger logger = LogManager.getLogger(TideExtractor.class);

    /**
     * This is the Mongo collection where the data will be written to
     */
    private MongoCollection mongoCollection = null;

    /**
     * This is the URL where the tide data resides
     */
    private String tideUrl = null;

    private SimpleDateFormat tideDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH);

    /**
     * The constructor
     *
     * @param mongoCollection
     * @param tideUrl
     */
    public TideExtractor(MongoCollection mongoCollection, String tideUrl) {
        // Set the mongo collection
        this.mongoCollection = mongoCollection;

        // Set the URL of the MEI data
        this.tideUrl = tideUrl;
    }

    /**
     * This method grabs the tide data from the tide URL and pushes it into a Mongo collection
     */
    public void extractData() {
        logger.debug("Starting extraction of Tide data");

        // Drop the collection (for now)
        mongoCollection.drop();

        // For the tide data, we will do this year by year from 1980 to now.  Grab the current year
        Calendar now = Calendar.getInstance();
        for (int i = 1980; i <= now.get(Calendar.YEAR); i++) {

            // Create start date parameter
            String beginDateParam = "&begin_date=" + i + "0101";

            // And end date
            String endDateParam = "&end_date=" + i + "1231";

            // Create the URL to the page
            URL website = null;
            try {
                logger.debug("URL call: " + tideUrl + beginDateParam + endDateParam);
                website = new URL(tideUrl + beginDateParam + endDateParam);
            } catch (MalformedURLException e) {
                logger.error("MalformedURLException caught trying to create URL from " + tideUrl + beginDateParam + endDateParam + ": " + e.getMessage());
            }

            // Check that we have a valid URL
            if (website != null) {
                // Now grab a connection
                URLConnection connection = null;
                try {
                    connection = website.openConnection();
                } catch (IOException e) {
                    logger.error("IOException trying to connect to " + tideUrl + ": " + e.getMessage());
                }

                // Check for the connection
                if (connection != null) {

                    // Create an InputStream
                    InputStream in = null;
                    try {
                        in = connection.getInputStream();
                    } catch (IOException e) {
                        logger.error("IOException caught trying to create InputStream from website: " + e.getMessage());
                    }

                    // Make sure we have the reader
                    if (in != null) {
                        // Create a byte array output stream to write entire results to
                        ByteArrayOutputStream baos = new ByteArrayOutputStream();

                        // And a buffer to hold streaming data
                        byte[] buf = new byte[8192];

                        // Now read in the entire response
                        int len = 0;
                        try {
                            while ((len = in.read(buf)) != -1) {
                                baos.write(buf, 0, len);
                            }
                        } catch (IOException e) {
                            logger.error("IOException trying to read tide data from URL");
                        }

                        String body = null;
                        try {
                            body = new String(baos.toByteArray(), "UTF-8");
                        } catch (UnsupportedEncodingException e) {
                            logger.error("UnsupportedEncodingException caught trying to convert page to string: " +
                                    e.getMessage());
                        }

                        // The list of documents that will be inserted
                        List<Document> documents = new ArrayList<Document>();

                        // Convert the JSON string to an object
                        JSONParser jsonParser = new JSONParser();

                        try {
                            JSONObject tideJSON = (JSONObject) jsonParser.parse(body);

                            // OK, if we are here, we have a year's worth of tidal data by hour. Grab the 'data' object
                            // and start shoving
                            JSONArray dataArray = (JSONArray) tideJSON.get("data");
                            for (int j = 0; j < dataArray.size(); j++) {
                                JSONObject dataObject = (JSONObject) dataArray.get(j);
                                // Try to parse the date
                                Date timestamp = null;
                                try {
                                    timestamp = tideDateFormat.parse(dataObject.get("t").toString());
                                } catch (ParseException e) {
                                    logger.error("ParseException caught trying to parse date: " + e.getMessage());
                                }
                                Float tide = Float.parseFloat(dataObject.get("v").toString());
                                Document newDocument = new Document("date", timestamp)
                                        .append("date-resolution", "hour")
                                        .append("name", "Mean Tide Level")
                                        .append("value", tide)
                                        .append("units", "Feet");

                                // Add it to the list
                                documents.add(newDocument);
                            }
                        } catch (org.json.simple.parser.ParseException e) {
                            logger.error("ParseException trying to parse the JSON string " + e.getMessage());
                        }

                        // Now insert them
                        logger.debug("Inserting documents for year " + i);
                        mongoCollection.insertMany(documents);
                    }
                }
            }
        }

        logger.debug("Done with MEI extraction");
    }
}