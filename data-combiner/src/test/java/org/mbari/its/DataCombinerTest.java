package org.mbari.its;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple DataCombiner.
 */
public class DataCombinerTest
        extends TestCase {
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public DataCombinerTest(String testName) {

        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {

        return new TestSuite(DataCombinerTest.class);
    }

    /**
     * Rigourous Test :-)
     */
    public void testApp() {

        assertTrue(true);
    }
}
